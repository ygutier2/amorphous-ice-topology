<div align="center">
<img width="80%" src="sketch.png">
</div>
</br>

# Amorphous-ice Topology

## The model
We use the TIP4P/2005 force-field to represent water molecules interactions. The integration of the equations of motion was carried out in GROMACS 2021.5 and in the NPT ensemble. Coulombic and Lennard-Jones interactions were calculated with a cutoff distance of 1.1 nm and long-range electrostatic interactions were treated using the Particle-Mesh Ewald (PME) algorithm. Temperatures and pressures are controlled using the Nose-Hoover thermostat with a time constant of 0.2 ps, and the Berendsen barostat with a time constant of 1 ps. Equations of motions are integrated with the Verlet algorithm with a time step of 1 fs.

The simulation protocol follows previous works employing the same water model. Low density amorphous (LDA) configurations were obtained by cooling liquid water equilibrated at T=300 K with a quenching rate of 1 K/ns down to T=100 K. Compression/decompression cycles were simulated with a compression/decompression rate of 0.01 GPa/ns at T=100 K, T=120 K and T=140 K. For each of these cases, the pressure was varied in the range $P\in$ [0.01, 2.0] GPa during compression, and $P\in$ [-0.5, 2.0] GPa during decompression, with a step of $\vert \Delta P \rvert=0.01GPa$. Therefore, we explored 450 different pressures for each temperature.

For details of the model and the system used we refer the reader to our paper: 

> (1) **Link to Densify: Topological Transitions and Origin of Hysteresis During the Compression and Decompression of Amorphous Ices**, *Yair Augusto Gutierrez Fosado,  Davide Michieletto and Fausto Martelli*, [**Phys. Rev. Lett. 133, 266102**, 2024](https://doi.org/10.1103/PhysRevLett.133.266102).

It is worth mentioning here that we have found similar topologies in other limited valence networks (in DNA hydrogels!):

> (2) **Topological Elasticity in Physical Gels with Limited Valence**, *Giorgia Palombo,  Simon Weir,  Davide Michieletto and Yair Augusto Gutierrez Fosado*, [arxiv, 2023](http://arxiv.org/abs/2308.09689). **Note:** you can find the data and scripts to perform the topology analysis for this system in the repository
>  [https://git.ecdf.ed.ac.uk/ygutier2/j1Topo](https://git.ecdf.ed.ac.uk/ygutier2/j1Topo)
>
> (3) **Nanostars planarity modulates the rheology of DNA hydrogels**, *Yair Augusto Gutierrez Fosado*, [**Soft Matter**, 2013, 19, 4820](https://doi.org/10.1039/D2SM00221C).


## What you will find in this repository:

* **1. Trajectories from similations**: Inside the folders *Pressure\_X* (with X/10000 the value of the pressure in GPa) you will find the file **fort.100** containing 11 thermally equilibrated configurations of the system described above and for a fixed temperature T=100K. In total, we provide configurations at six different pressures during compression: P=0.01, 0.4, 0.8, 1.2, 1.6 and 2.0 GPa.
* **2. Analysis**: The configurations provided are analysed using the scripts inside *ANALYSIS\_MASTERFILES*, where we explain how to compute the connectivity of the network and the topology analysis.

Please look at the README files inside these folders as they provide further details of the implementation of the analysis. This repository has been created thinking in the easy execution of the codes through *Bash scripts* that are inside *ANALYSIS\_MASTERFILES* and contain all the commands you would normally type in the terminal. We hope that this makes easy to track all the steps in our analysis. To set the executing-permission of the scripts and run the scripts, you will need to type in the terminal: *chmod a+x bashScriptName* and *./bashScriptName*, respectively.

**IMPORTANT:** It is worth mentioning here that this analysis is computational expensive, and therefore, if the user wants to analyse several time-frames and/or pressures, we recommend to use SLURM to run the analysis for multiple identical serial cases (each one representing a pressure) in parallel. The codes we used for this type of analysis can be found in *ANALYSIS\_MASTERFILES\_SLURM* and they are a sort of generalization to the codes found in *ANALYSIS\_MASTERFILES*.

## Software requirements
* A c++14-compliant compiler (tested with GCC >= 5.4)
* [Mathematica 13](https://www.wolfram.com/mathematica/?source=nav). **IMPORTANT:** for the easy application of our codes to analyse several pressures, we use a text‐based interface of mathematica (.m files) and not the usual notebook (.nb files). In this way, the instructions in the mathematica script go straight to the kernel. The way that you typically execute a mathematica script in a terminal is through the command: *math -script ScriptName.m* However, for the analysis of a few cases, like the six-pressures provided in this repository, it is also possible to use the usual graphical interface of Mathematica (further details in the analysis folder).
* [AWK](https://www.gnu.org/software/gawk/).
* [VMD](https://www.ks.uiuc.edu/Research/vmd/) for visualization purposes (not analysis).
* All analysis scripts have been tested in a PC with operating system **Ubuntu 20.04.6 LTS**. However, it is expected that in any other linux version, the scripts should work.

## Citing Amorphous Ice Analysis Tools

If you find useful the codes deposited here, please cite the papers mentioned above. If you feel appropiate, in addition you can also mention the URL of this repository [https://git.ecdf.ed.ac.uk/ygutier2/amorphous-ice-topology](https://git.ecdf.ed.ac.uk/ygutier2/amorphous-ice-topology)