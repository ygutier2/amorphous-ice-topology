# Equilibrated configurations at 1.6 GPa

The file **fort.100** contains the trajectory (11 independent snapshots of the system) from simulations in the xyz format. Each snapshot starts with an empty line followed by the box size. You then have the x-y-z positions of atoms, following the sequence: oxygen, hydrogen, hydrogen, repeated 512 times (one per each water molecule). **Note:** the origin of coordinates, does not lie at the centre of the box, so

> 0 < Lx < L
> 
> 0 < Ly < L
> 
> 0 < Lz < L

Instructons to perform the analysis can be found inside the folder *../ANALYSIS\_MASTERFILES*. During the analysis stage, three folders will be created here:

> 1\_network\_connectivity
> 
> 2\_Loops
> 
> 3\_Linking