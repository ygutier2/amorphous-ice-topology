# Network Connectivity

The C++ code **connectivity\_vs\_t.cpp** computes:

1. The file **nc\_vs\_t.dat** with the total number of hydrogen bonds found for each of the eleven frames.
2. The file **traj.lammpstrj** with the 11 frames contained in the file *fort.100* and written in a compatible format with VMD. If you want to visualize the 11 configurations simply type in a terminal *vmd traj.lammpstrj*.
3. The files **network\_connectivity\_frameX.dat** (with X representing the corresponding frame). Each of these 11 files contains four columns. The first two columns indicate the moleculeID ($m\in[1,M]$) of water molecules that form a hydrogen bond. The last two columns contain the particleID of the hydrogen and oxygen through which water molecules are linked.
4. The files **angle_dist_frameX.dat** (with X representing the corresponding frame). Each of these 11 files contains two columns with the value of the angles and distance when there is a hydrogen bond.

Then, we run the bash script **bash_distributions.sh**. This will first make use of the AWK script **occurrences.awk**, which reads the **network\_connectivity\_frameX.dat** files, and then it creates the files **foccurrences_frameX.txt** (with X representing the corresponding frame). Each of these 11 files contains two columns. The first one is the moleculeID ($m\in[1,M]$) of water molecules, and the second one is the number of contacts with the rest of the molecules. The previous files are then used to compute **frequency_frameX.txt** which contain in the first column the valence, and in the second column the frequency of that valence. All these files are stored inside the folder *Valence/*.

Finally, the AWK scripts **awk.hist_distHB** and **awk.hist_thetaHB** are used to read the files **angle_dist_frameX.dat**. With this information, the probability distribution functions of angles and distances of the HB are created in **distprobAngleX** and **istprobDistX**. All these files are stored inside the foder **Distributions/**


