#!/bin/bash
#
#SBATCH --job-name=water.analysis.comp.part6.T100K
#SBATCH --partition=long
#SBATCH --time=168:00:00
#SBATCH --mem-per-cpu=4096
#This means run 50 single CPU jobs in batches
#SBATCH --array=1-50


#The pressure to analyse during compression: pi=400, dp=400, pf=20000. A total of 50
dp=400
p=$(($dp*${SLURM_ARRAY_TASK_ID}))


#Print date and time when the analysis started
pdt1=$(date '+%d/%m/%Y %H:%M:%S');
echo "Starting time: $pdt1"

#The taskID:
echo "Hello from SLURM task: ${SLURM_ARRAY_TASK_ID}"
  
#Do the analysis
echo "Finding large loops"
./bash.analysis6.LargeLoops.sh ${p}
echo "Finish Finding large loops"

#Print date and time when the analysis finished
pdt2=$(date '+%d/%m/%Y %H:%M:%S');
echo "Ending time: $pdt2"

mv slurm-*_${SLURM_ARRAY_TASK_ID}.out outputSlurm6/

