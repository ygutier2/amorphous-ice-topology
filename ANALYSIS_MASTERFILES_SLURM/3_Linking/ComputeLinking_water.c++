#include<vector>     /*Library for using vector insted of arrays*/
#include<iostream>   /*Basic functions*/   
#include <cmath>     /*Define some fundamental mathematical operations*/
#include<stdlib.h>   /*Useful for random numbers*/
#include <sstream>
#include <fstream>   /*To read and write into files*/
#include <limits>
#include <stdio.h>   /*Basic functions in particular "sprintf"*/
#include <iomanip>   // std::setprecision
#include <algorithm> //max element


using namespace std;

#define pi 3.141592653589793238462643383279502884197169399375
#define SMALL 1e-10

//Define the maximum number of minloops we will use
const int Nmax=40000;

//Define the maximum number of particles in each minimum-loop.
//Remember that the Mathematica script found minimum-loops made by at the most 20 water molecules.
//This sets 20*3=60. A bigger number will work
const int Nbeadsmax=60;

float gaussLnkNumb(int n1, int n2, float c1[][3], float c2[][3]);

int main(int argc, char *argv[])
{

    /**********************************************/
    /*Pass the following arguments to the programm*/
    /**********************************************/
    /*To get help screen of the program: ./trajectories help*/
    if(argc<6 || argc>6)
    {
        cout << "Four arguments are required by the programm:"  << endl;
        cout << "1.- Path to the input data"                    << endl;
        cout << "2.- Output path"                               << endl;
        cout << "3.- Input file (without time-frame)"           << endl;
        cout << "4.- Time-frame"                                << endl;
        cout << "5.- Print linked configurations at this time-frame"                                << endl;
    }


if(argc==6)
{
        
    //cout << "ok" << endl;
    /*argv[1] Directory where the data is*/
    /*argv[2] Directory where the output will be stored*/
    /*argv[3] Input file*/
    
    //First file to read contains: (1) time-frame, (2) l; (3)-(3+l-1)Particle-ids
    int frame;
    frame = atoi(argv[4]);
    
    int td;
    td = atoi(argv[5]);
    
    int l;
    long int id;
        
    //Vector containing the ParticleIDs in each of the loops
    vector<int> mlSize;

    //The total number of unrepeated loops
    int Nloops;
    
    //Matrix containing the ids in each loop
    vector< vector<float> > mlIDs(Nmax, vector<float>(Nbeadsmax));
    
    //These variables are important to read the second file
    //The total number of atoms
    int Natoms;
    
    //Each water molecule is made by two hydrogens and one oxigen.
    //So the number of particles per molecule is 3:
    int Nppmol=3;
    
    //The number of molecules: Natoms/Nppmol
    int M;
    
    
    //When one ring is linked by other two (or more). It could happen that the latter two rings shared beads.
    //This could potentially leads to an overcount of Lk. Here we define the number of shared beads between the two latter rings to decide removing the counting of the extra LK
    int Nsbcutoff=6;
    
    //Declaration for linking matrix:    
    //Wrapped position of the particles in each loop
    vector< vector< vector<float> > > positionOriginal(Nmax, vector<vector<float> >(Nbeadsmax, vector<float>(3)));
    //Reconstructed positions through the PBC
    vector< vector< vector<float> > > position(Nmax, vector<vector<float> >(Nbeadsmax, vector<float>(3)));
    
    //This is a 2D vector which which rows will represent:
    //1) minloopID1
    //2) minloopID2
    //3) Lk betewwn the two minimum loops
    //The columns will be the cases for which LK is different from zero
    vector< vector<float> > LinkingMap(3, vector<float>());


    /*******************************************************************/
    /**Read first input file with the IDS of beads in the minimum loop**/
    /*******************************************************************/
    char readFile[100] = {'\0'};
    sprintf(readFile, "1_dataLoop/LoopsParticleIDs_t%d.dat",frame);
    ifstream read(readFile);
    
    //Loop over rings
    int ml=0;
 
    if(!read){cout << "Error while opening data file!"<<endl;}
    if (read.is_open())    
    {                   
        string line;
        string dummy;                

        //Loop to read the file, line by line into a string.
        while (getline(read, line))            
        {                            
            //To split the line into its corresponding values
            istringstream is(line);
            
            //Reads the number of beads forming the loop
            is >> l;
            mlSize.push_back(l);
            
            //Reads the ParticleID in the loop
            for(int j=0; j<l; j++){
               is >> id;
               mlIDs[ml][j]=id;
            }            
            ml+=1;
        }
    }
    read.close();
    
    //The total number of minimum loops.
    Nloops=mlSize.size();
    cout << "Nloops=" << Nloops << endl;
    
    //The number of particles in the largest loop
    int Nbmax=*max_element(mlSize.begin(), mlSize.end());
    cout << "Nbmax=" << Nbmax << endl;
    
    
    
    /********************************************************/
    /**Reads input file with the positions of all particles**/
    /********************************************************/
    ifstream indata1;
    char readFile1[400] = {'\0'};
    string dummy;
    string line;
    sprintf(readFile1, "%s%s%d",argv[1],argv[3],frame);
    indata1.open(readFile1);
    
    /*the following variables will be useful to read the data file*/
    long long int time;
    long int mol;
    char type;
    float x,y,z;
    float lmin,lmax,Lx,Ly,Lz;
    
    vector<vector<float> > AllPositons;

    if (indata1.is_open())
    {  
        //READ INFO
        int i=0;
        int fflag=0;
        while (!indata1.eof()){
            //Read headers
            if(i==1) {
                indata1 >> time;
                //cout << "time " << time <<endl;                    
            }
            
            if(i==3) {
                indata1 >> Natoms;
                //cout << "Natoms " << Natoms << endl;
                
                AllPositons.resize(Natoms, vector<float>(3));                                
            }
            
            if(i==5) {
                indata1 >> lmin >> lmax;
                Lx = lmax-lmin;
                //cout << "Lx " << Lx <<endl;
            }

            if(i==6) {
                indata1 >> lmin >> lmax;
                Ly = lmax-lmin;
                //cout << "Ly " << Ly <<endl;
            }

            if(i==7) {
                indata1 >> lmin >> lmax;
                Lz = lmax-lmin;
                //cout << "Lz " << Lz<<endl;
            }
            
            //READ ATOMS
            if(i>=9) {
                indata1 >> id >> mol >> type >> x >> y >> z;
            
                //cout << id << " " << mol << " " << type << " " << x << " " << y << " " << z << endl;
                AllPositons[id-1][0] = x;
                AllPositons[id-1][1] = y;
                AllPositons[id-1][2] = z;            
            }

            else getline(indata1,dummy);        
            i+=1;
        }
        indata1.close();        
        indata1.clear();  
    }
   
    else{cout<<"error reading file: " << readFile1 << endl;}    
    
 
    /*************************************************/
    /**Pass the positions of particles in loops only**/
    /*************************************************/
    for(int m=0;m<Nloops;m++){
        //Number of particles in the mth-loop:
        l=mlSize[m];
        //cout << l;
        
        for(int n=0; n<l;n++){
            //IDs of particles in the loop
            id=mlIDs[m][n];
            x=AllPositons[id-1][0];
            y=AllPositons[id-1][1];
            z=AllPositons[id-1][2];
                        
            //This is static --> will never change
            positionOriginal[m][n][0]=x;
            positionOriginal[m][n][1]=y;
            positionOriginal[m][n][2]=z;
            
            //cout << " " << id;
        }
        //cout << endl;
    }
    
    
    ///////////////////////////////////////////////
    //COMPUTE LINKING NUMBER BETWEEN PAIR OF LOOPS/
    ///////////////////////////////////////////////    
    float TotalLinking=0;
    float Totalrepeatedlk=0;
    float lk=0;
    float Curve1[Nbeadsmax][3];
    float Curve2[Nbeadsmax][3];
    
    for(int m1=0;m1<Nloops-1;m1++){
        //cout << "computing linking for minloopID=" << m1 << endl;
        //////////////////////////////
        //Set the first minimum loop//
        //////////////////////////////
        //Number of beads in the first minimum loop (m1)
        int s1=mlSize[m1];
        
        //Set the position vector to the original position
        for(int n=0; n<s1;n++){
            position[m1][n][0]=positionOriginal[m1][n][0];
            position[m1][n][1]=positionOriginal[m1][n][1];
            position[m1][n][2]=positionOriginal[m1][n][2];
        }
        
        //Reconstruct the first minimum loop through PBC//
        //Maximum distance between consecutive particles in a loop
        float rc=3.5;
        
        for(int n=0; n<s1-1;n++){
            float dx = position[m1][n+1][0] - position[m1][n][0];
            float dy = position[m1][n+1][1] - position[m1][n][1];
            float dz = position[m1][n+1][2] - position[m1][n][2];
            
            float dist=sqrt(dx*dx + dy*dy + dz*dz);
            
            if(dist>rc){
                /*Minimum Image criterion*/
               	if (dx > 0.5*Lx){ dx -= Lx;}
                if (dy > 0.5*Ly){ dy -= Ly;}
           	    if (dz > 0.5*Lz){ dz -= Lz;}

           	    if (dx <= -0.5*Lx){ dx += Lx;}
           	    if (dy <= -0.5*Ly){ dy += Ly;}
           	    if (dz <= -0.5*Lz){ dz += Lz;}

           	    //change the position of the n+1 bead
           	    position[m1][n+1][0] = position[m1][n][0]+dx;
           	    position[m1][n+1][1] = position[m1][n][1]+dy;
         	      position[m1][n+1][2] = position[m1][n][2]+dz;
            }
        }
                
        //Pass the curve representing the first loop
        for(int nb=0;nb<s1;nb++){
            for(int d=0;d<3;d++){
                Curve1[nb][d]=position[m1][nb][d];
            }
        }
        
        //Vector to store the ringID of all the loops (m2) linked with the first loop (m1)
        vector <int> linkedRings;
        
        
        ///////////////////////////////
        //now look at all other loops//
        ///////////////////////////////
        for(int m2=m1+1;m2<Nloops;m2++){
            //number of beads in the second minimum loop
            int s2=mlSize[m2];
            
            //Set the position vector to the original position
            for(int n2=0; n2<s2;n2++){
                position[m2][n2][0]=positionOriginal[m2][n2][0];
                position[m2][n2][1]=positionOriginal[m2][n2][1];
                position[m2][n2][2]=positionOriginal[m2][n2][2];
            }                   
                
            //Now reconstruct the minimum loop through PBC. 
            for(int n2=0; n2<s2-1;n2++){
                float dx = position[m2][n2+1][0] - position[m2][n2][0];
                float dy = position[m2][n2+1][1] - position[m2][n2][1];
                float dz = position[m2][n2+1][2] - position[m2][n2][2];
            
                float dist=sqrt(dx*dx + dy*dy + dz*dz);
            
                if(dist>rc){
                    /*Minimum Image criterion*/
                   	if (dx > 0.5*Lx){ dx -= Lx;}
                    if (dy > 0.5*Ly){ dy -= Ly;}
               	    if (dz > 0.5*Lz){ dz -= Lz;}

               	    if (dx <= -0.5*Lx){ dx += Lx;}
               	    if (dy <= -0.5*Ly){ dy += Ly;}
               	    if (dz <= -0.5*Lz){ dz += Lz;}

               	    //change the position of the i+1
               	    position[m2][n2+1][0] = position[m2][n2][0]+dx;
               	    position[m2][n2+1][1] = position[m2][n2][1]+dy;
             	      position[m2][n2+1][2] = position[m2][n2][2]+dz;
                }
            }
                                
            //Pass the curve representing the second loop
            for(int mb=0;mb<s2;mb++){
                for(int d=0;d<3;d++){
                    Curve2[mb][d]=position[m2][mb][d];
                }           
       	    }
                
            ////////////////////////////////////
            //Compute Lk between pair of curves/
            ////////////////////////////////////
            //lk=0 when not linked;
            //lk=-nan when the two rings share some beads
            if(m1!=m2){
                lk=gaussLnkNumb(s1,s2,Curve1,Curve2);
                
                //If lk=nan set lk to zero
                if (lk != lk){lk=0;}
            }
                
                
            //If lk=0, then continue with the next second minimum loop
            //if(lk==0){continue;}
                
            if(lk!=0){
                //cout << "ring1=" << m1+1 << " ring2=" << m2+1 << " lk=" << lk << endl;
            
                TotalLinking+=abs(lk);
                   
                LinkingMap[0].push_back(m1);
                LinkingMap[1].push_back(m2);
                LinkingMap[2].push_back(lk);
                    
                //Store the ringID of all the m2-rings linked with the m1-ring.
                linkedRings.push_back(m2);
                
                //Print configurations at this frame (td)    
                if(frame==td){
                    //Write the reconstructed configurations of the two rings that are linked
				    //UNCOMMENT TO WRITE THE CONFIGURATIONS BUT IT OCCUPIES A LOT OF SPACE
					/*
                    stringstream writeFile1;
                    writeFile1 <<"linkedConfigurations/LinkedConfigs_"<< frame << "_ring1-" << m1+1 << "_ring2-" << m2+1 << ".xyz";
                    ofstream write1(writeFile1.str().c_str());
                    write1 << s1+s2<< endl;
                    write1 << "Atoms. frame : 0"<< endl;
                    for(int nb=0;nb<s1;nb++){
                        write1 << "C "  << Curve1[nb][0] << " " << Curve1[nb][1] << " " << Curve1[nb][2] <<endl;
                    }
                    for(int mb=0;mb<s2;mb++){
                        write1 << "O "  << Curve2[mb][0] << " " << Curve2[mb][1] << " " << Curve2[mb][2] <<endl;
                    }
                    write1.close();
                    write1.clear();
					*/
                
                    //Write the pariticleIDS in the two minimum loops that are linked
                    stringstream writeFile2;
                    writeFile2 <<"linkedIDS/LinkedIDs_" << frame <<  ".dat";                
                    ofstream write2(writeFile2.str().c_str(), std::ios_base::app);
                
                    write2 << "ring1=" << m1+1 << ", ring2=" << m2+1 << endl;
                    for(int nb=0;nb<s1;nb++){
                        if(nb==0){write2 << mlIDs[m1][nb];}
                        if(nb>0) {write2 << " " << mlIDs[m1][nb];}
                    }
                    write2 << endl;
                
                    for(int mb=0;mb<s2;mb++){
                        if(mb==0){write2 <<  mlIDs[m2][mb];}
                        if(mb>0) {write2 << " " << mlIDs[m2][mb];}
                    }
                    write2 << endl;
                    write2 << endl;
                }//closes if to write files at frame=1       
            }//Closes if lk!=0
        }//close loop on m2. Until here we found all the m2-rings that are linked to the m1-ring
        
        
        //////////////////////////////////////////////////////////
        //We want to know if any of these m2-rings have repeated//
        //beads that are contributing to the overcounting of Lk //
        //////////////////////////////////////////////////////////        
        //Number of m2-rings linked with m1-ring
        int Nlkrings=linkedRings.size();
        
        //ID of the m2-rings that are contributing to the overcounting of LK by shared beads
        vector<int>repID;        
        
        for(int nr1=0;nr1<Nlkrings-1;nr1++){
            //The ringID of the linked ring:
            int rID1=linkedRings[nr1];
            
            //The number of beads forming this ring
            int Nbring1=mlSize[rID1];
            
            //The ID of beads in the ring
            vector<int> r1(Nbring1);
            for(int i=0;i<Nbring1;i++){
                r1[i]=mlIDs[rID1][i];
            }
            
            //Sort the r1 vector
            sort(r1.begin(), r1.end());
                        
            //Loop over the other m2-linked rings to see if they have repeated beads
            for(int nr2=nr1+1;nr2<Nlkrings;nr2++){
                //The ringID of the linked ring:
                int rID2=linkedRings[nr2];
                
                //The number of beads forming this ring
                int Nbring2=mlSize[rID2];
                
                //The ID of beads in the ring
                vector<int> r2(Nbring2);
                for(int i=0;i<Nbring2;i++){
                    r2[i]=mlIDs[rID2][i];
                }
                
                //Sort the r2 vector
                sort(r2.begin(), r2.end());
                
                //Compare the elements of the vector r1 and r2. We use "binary_search" to see if the components of r2 exist in r1.
                int Nsharedbeads=0;
                for (auto componente : r2) //-->BREAK BELOW TAKES YOU OUT THIS LOOP
                {
                    //cout << "Searching for " << component << endl;
                    if (binary_search(r1.begin(), r1.end(), componente)){
                        //cout << "Found " << componente << endl;
                        Nsharedbeads+=1;
                    }

                    //else{ cout << "component not found" << endl;}
                    
                    //If you find that at least Nsbcutoff=6 beads are shared then
                    if(Nsharedbeads==Nsbcutoff){
                        repID.push_back(rID2);
                        break;
                    }
                }
            }        
        }//close loop over linked rings
        
        //Now we want to erase the repeated elements in the vector called repID
        sort( repID.begin(), repID.end() );
        repID.erase( unique( repID.begin(), repID.end() ), repID.end() );
        
        //The size of the vector represents the number of over counted Lk with the m1-ring
        Totalrepeatedlk+=repID.size();
        
        //Write the pariticleIDS of the first minloop (m1) and the m2-linked-rings to m1 that are repeated.
        if(frame==td){
            stringstream writeFile3;
            writeFile3 <<"linkedRepeatedIDS/LinkedRepeatedIDs_" << frame <<  ".dat";
            ofstream write3(writeFile3.str().c_str(), std::ios_base::app);
            
            for(int i=0; i<repID.size(); i++){                
                write3 << "ring1=" << m1+1 << ", ring2Reapeated=" << repID[i]+1 << endl;
                for(int nb=0;nb<s1;nb++){
                    if(nb==0){write3 << mlIDs[m1][nb];}
                    if(nb>0) {write3 << " " << mlIDs[m1][nb];}
                }
                write3 << endl;
                
                int s2=mlSize[repID[i]];
                for(int mb=0;mb<s2;mb++){
                    if(mb==0){write3 <<  mlIDs[repID[i]][mb];}
                    if(mb>0) {write3 << " " << mlIDs[repID[i]][mb];}
                }
                write3 << endl;
                write3 << endl;
            }
        }
                
    }//close loop on m1
    
    cout << "time t= " << frame << " -> Nlinking = " <<  TotalLinking << " -> NrepeatedLk = " << Totalrepeatedlk << endl;
    
    //Print results at a given time
    stringstream writeFile4;
    writeFile4 << "Linking_vs_time.dat";
    ofstream write4(writeFile4.str().c_str(), std::ios_base::app);
    write4 << frame << " " << Nloops << " " << TotalLinking << " " << Totalrepeatedlk << endl;
       
    //Print Full linking matrix
    stringstream writeFile3;
    writeFile3 <<"LinkingMatrix.dat";
    ofstream write3(writeFile3.str().c_str(), std::ios_base::app);
    for(int i=0; i<LinkingMap[0].size();i++){
        write3 << frame << " " << LinkingMap[0][i] << " " << LinkingMap[1][i] << " " << LinkingMap[2][i] << endl;
    }

}


return 0;
}








float gaussLnkNumb(int n1, int n2, float c1[][3], float c2[][3]){
    float lk=0;
    long double num=0;
    float dr1[3],dr2[3],r[3],cross[3];
    long double dist, dist3;
    for(int n=0 ;n<n1; n++){
        //Flag to exit the loop
        int flag1=0;
        for(int m=0;m<n2;m++){
            r[0] = c2[m][0] - c1[n][0];
            r[1] = c2[m][1] - c1[n][1];
            r[2] = c2[m][2] - c1[n][2];
            dist = sqrt(r[0]*r[0] + r[1]*r[1] + r[2]*r[2]);
            dist3 = dist*dist*dist;
            
            if (dist>0 && dist<SMALL){
                //cout<<"distance between beads in lk calculation is too short "<< dist << endl;
                dist=0.0;
            }                         
            
            if(dist>0){      
    	           r[0]=-r[0];r[1]=-r[1];r[2]=-r[2];

                dr1[0] = c1[(n+1)%n1][0] - c1[n][0];
                dr1[1] = c1[(n+1)%n1][1] - c1[n][1];
                dr1[2] = c1[(n+1)%n1][2] - c1[n][2];
       
                dr2[0] = c2[(m+1)%n2][0] - c2[m][0];
                dr2[1] = c2[(m+1)%n2][1] - c2[m][1];
                dr2[2] = c2[(m+1)%n2][2] - c2[m][2];
       
                cross[0] = dr1[1]*dr2[2] - dr1[2]*dr2[1];
                cross[1] = dr1[2]*dr2[0] - dr1[0]*dr2[2];
                cross[2] = dr1[0]*dr2[1] - dr1[1]*dr2[0];
       
                num = r[0]*cross[0] + r[1]*cross[1] + r[2]*cross[2];
       
                lk += num*1.0/dist3;
            }
        
            //This happens when two minimum loops share some beads    
            if(dist==0){lk=0; flag1=1; break;}
        }
        
        //Exit the calculation because minimum loops share beads
        if(flag1==1){break;}
    }
    return round(lk*1.0/(4.0*M_PI));
}
