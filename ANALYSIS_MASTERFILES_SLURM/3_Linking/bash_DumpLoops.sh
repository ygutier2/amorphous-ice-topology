#!/bin/bash
  
###################################
#Copy file from previous analysis#
###################################
#The file "fractalDimension2.dat" with the following information regarding the unrepeated loops (formed by at the most 13 water-molecules)
#1.- time-frame
#2.- The number of water-molecules (l) connected in the loop that passes through vertex (r1)
#3.- The Rg of the loop
#4.- The next columns represent the molecule-id of all water-molecules connected (in order, from r1).
cp ../2_Loops/fractalDimension2.dat .
mkdir -p 1_dataLoop


#############################################################
#Here we compute the particle ID of all Oxygens forming loops
#############################################################
 #1.- Path to the input data
 datapath="../2_Loops/2_splitTraj/"

 #2.- Output path
 outputpath="1_dataLoop/"

 #3.- Input file with configuration information (without timestep)
 rfile="Conf"

 # READ Variables from the file Loops_cleaned.txt#
 #################################################
 awk -v dp="${datapath}" -v op="${outputpath}" -v rf="${rfile}" '{
   #$1=timestep, rc=2.2, ac=30, $2=l
   arguments = dp" "op" "rf" "$1" "2.2" "30" "$2;
   
   for (i = 4; i <= NF; i++) {
     col[i] = $i;
     
     arguments = arguments" "col[i];     
   }
   
   #print "Arguments:", arguments;
   # Execute the C++ program and pass the concatenated values as a single argument
   cmd = "./LoopsIDs.out " arguments;
   system(cmd);
 }' fractalDimension2.dat
 
 rm LoopsIDs.out
 rm fractalDimension2.dat 

 #This programm will create a file per timestep analysed  (LoopsParticleIDs_t*) with the following columns:
 #1.-The total number of particles forming the loop
 #2.-The ParticleID of consecutive particles in the loop (oxigens only)
