#!/bin/bash
  #What is the pressure you want to analyse:
  p="$1"
  
  #Current working directory
  cwd=$(pwd)
  
  
  #################################
  ## ANALYSIS DURING COMPRESSION ##
  #################################
  echo "Looking for knots during compression at P=${p}"
      
  #move terminal to the analysis folder and remove previous analysis
  cd ../Pressure_${p}/                         
  cd 4_Knotting/
      
  #Run bash to extract the 3D-positions of particles in the loops
  ./bash.Extract3Dloops.sh
      
  #Move back to MASTER folder
  cd "${cwd}"
