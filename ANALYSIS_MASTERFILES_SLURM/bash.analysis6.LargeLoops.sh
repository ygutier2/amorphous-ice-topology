#!/bin/bash
  #What is the pressure you want to analyse:
  p="$1"
  
  #Current working directory
  cwd=$(pwd)
  
  
  #################################
  ## ANALYSIS DURING COMPRESSION ##
  #################################
  echo "Looking for knots during compression at P=${p}"
      
  #move terminal to the analysis folder and remove previous analysis
  cd ../Pressure_${p}/
  if [ -e "4_Knotting" ]; then
     rm -r 4_Knotting
  fi
                        
  #Copy folder to the corresponding directory
  cp -r "${cwd}/4_Knotting" .
  cd 4_Knotting
      
  #Run mathematica script in the terminal
  math -script NminLoop_v2.m
      
  #Move back to MASTER folder
  cd "${cwd}"
