#!/bin/bash
  #What is the pressure you want to analyse:
  p="$1"
 
  #Current working directory
  cwd=$(pwd)
  
  #################################
  ## ANALYSIS DURING COMPRESSION ##
  #################################
  echo "Hello from bash1, task: ${SLURM_ARRAY_TASK_ID}"
  echo "Network connectivity during compression at P=${p}"
     
  #move terminal to the analysis folder
  cd ../Pressure_${p}/
  if [ -e "1_network_connectivity" ]; then
     rm -r 1_network_connectivity
  fi
            
  #1.- Path to the input data (data file in same folder)
  datapath="../"
      
  #2.- Output path
  outputpath="./"
      
  #3.- Input file with atoms information
  rfile="fort.100"
      
  #The total number of files inside the DUMP folder
  nf=$(ls -lorth ${rfile} | wc -l)
  #echo "The total number of data files at p=${p} is:  ${nf}"
      
  #4.- Cutoff distance between H(A)--O(B), in Angstroms
  rc="2.2"
      
  #5.- Cutoff angle between O(B)O(A)-axis and H(A)O(B)-bond, in degrees
  ac="30"
      
  #Copy folder for the analysis inside the corresponding pressure
  cp -r "${cwd}/1_network_connectivity" .
  cd 1_network_connectivity
    
  if [ "$nf" -gt 0 ]
  then
      ./connectivity ${datapath} ${outputpath} ${rfile} ${rc} ${ac}
  fi
  
  #############
  ## Valence ##
  #############
  ./bash_distributions.sh
      
  #Move back to folder
  cd "${cwd}"