#!/bin/bash
  #What is the pressure you want to analyse:
  p="$1"
  
  #Current working directory
  cwd=$(pwd)
  
  
  #################################
  ## ANALYSIS DURING COMPRESSION ##
  #################################
  echo "Hello from bash4, task: ${SLURM_ARRAY_TASK_ID}"
  echo "Linking during compression at P=${p}"
      
  #move terminal to the analysis folder and remove previous analysis
  cd ../Pressure_${p}/
  if [ -e "3_Linking" ]; then
     rm -r 3_Linking
  fi
                        
  #Copy folder to the corresponding directory
  cp -r "${cwd}/3_Linking" .
  cd 3_Linking
      
  #Run bash to extract the ID of all particles in the loops     
  ./bash_DumpLoops.sh
      
  #Move back to MASTER folder
  cd ${cwd}
