#!/bin/bash
#
#SBATCH --job-name=water.analysis.comp.part4.T100K
#SBATCH --partition=long
#SBATCH --time=168:00:00
#SBATCH --mem-per-cpu=2048
#This means run 200 single CPU jobs in batches of 64
#SBATCH --array=1-200%64


#The pressure to analyse during compression: pi=100, dp=100, pf=20000. A total of 200 
dp=100
p=$(($dp*${SLURM_ARRAY_TASK_ID}))


#Print date and time when the analysis started
pdt1=$(date '+%d/%m/%Y %H:%M:%S');
echo "Starting time: $pdt1"

#The taskID:
echo "Hello from SLURM task: ${SLURM_ARRAY_TASK_ID}"

#Do the analysis
echo "Extracting IDs of oxygens in loops slurm"
./bash.analysis4.linking.IDs.sh ${p}
echo "Finish Extracting IDs of oxygens in loops slurm"

#Print date and time when the analysis finished
pdt2=$(date '+%d/%m/%Y %H:%M:%S');
echo "Ending time: $pdt2"

mv slurm-*_${SLURM_ARRAY_TASK_ID}.out outputSlurm4/
