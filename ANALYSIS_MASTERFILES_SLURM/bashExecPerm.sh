#!/bin/bash

   echo "Executing permission:" 
   
   echo "Slurm files:"        
	  chmod a+x slurm.parallel.part1.sh
	  chmod a+x slurm.parallel.part2.sh
	  chmod a+x slurm.parallel.part3.sh
	  chmod a+x slurm.parallel.part4.sh
	  chmod a+x slurm.parallel.part5.sh
  	chmod a+x slurm.parallel.part6.sh
  	chmod a+x slurm.parallel.part7.sh
  	chmod a+x slurm.parallel.part8.sh
	
	  echo "Main bash scripts:"
	  chmod a+x bash.analysis1.networkconnectivity.sh
	  chmod a+x bash.analysis2.loops.mathematica.sh
	  chmod a+x bash.analysis3.loops.fractalDimension.sh
	  chmod a+x bash.analysis4.linking.IDs.sh
	  chmod a+x bash.analysis5.linking.sh
	  chmod a+x bash.analysis6.LargeLoops.sh
	  chmod a+x bash.analysis7.knotting.sh
	  chmod a+x bash.analysis8.kymoknot.sh
	  
	  echo "Inside 1_network_connectivity"
   cd 1_network_connectivity
   c++ -std=c++11 connectivity.cpp -o connectivity
	  chmod a+x bash_distributions.sh
   chmod a+x occurrences.awk
	  chmod a+x awk.hist_distHB
	  chmod a+x awk.hist_thetaHB
	  cd ../

   echo "Inside 2_Loops"
	  cd 2_Loops/
	  chmod a+x bash_NumberOfMinLoops.sh
	  chmod a+x awk.remove_repeated_lopps.sh
   c++ -std=c++11 splitTraj.cpp -o splitTraj
   c++ fractalD_ring_v2.cpp -o fractalD_ring_v2.out -lm
   c++ loops_distribution.cpp -o loops_distribution.out -lm
   chmod a+x awk.ave_at_same_l.sh
   cd ../
   
   echo "Inside 3_Linking"
   cd 3_Linking/
	  chmod a+x bash_DumpLoops.sh
	  chmod a+x bash_computeLk.sh
	  c++ LoopsIDs.cpp -o LoopsIDs.out -lm
	  c++ -std=c++11 ComputeLinking_water.c++ -o ComputeLinking_water
	  cd ../
	  
	  echo "Inside 4_Knotting"
	  cd 4_Knotting/
	  chmod a+x bash.Extract3Dloops.sh
	  chmod a+x bash.kymoKnot.sh
	  c++ dataFormatKymoknot.c++ -o dataFormatKymoknot.out -lm
   cd ../

  echo "Creating outputSlurm-i (with i running from 1 to 8, for every part of the analysis) where the output files from slurm will be saved."
  echo "his is useful just to keep track of the analysis."
  mkdir outputSlurm1
  mkdir outputSlurm2
  mkdir outputSlurm3
  mkdir outputSlurm4
  mkdir outputSlurm5
  mkdir outputSlurm6
  mkdir outputSlurm7
  mkdir outputSlurm8
	
	
	
	

   
	  
	  
   
   
	  
	  

