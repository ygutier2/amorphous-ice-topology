# Knotting analysis
## The mathematica notebook

The mathematica notebook **NminLoop\_v2.m** reads the data from the network connectivity files in folder **../1\_networkConnectivity/**. These files contain information about which water molecule (with  moleculeID $m\in[1,N]$) is connected with any other water molecule. The mathematica notebook contains commands to look for the loops passing through the vertex/molecule $r_{1}$ and formed by exactly $l=25$ water molecules. To improve the efficiency of our analysis, we find at the most $N_{loopMax}=500 000$ loops passing through vertex $r_{1}$, then, we remove that vertex and continue our loop-search at vertex $r_{1}+1$. 

**IMPORTANT**: before running the mathematica notebook *NminLoop\_v2.m* you need to replace the value of the variables:

1. **Ntot**: the total number of particles in the simulation, for our case 512X3=1536.
2. **ti**: This is the initial frame to analyse. Since in our trajectory file we have 11 frames, then $t_{i} \in [1,11]$. 
3. **tf**: This is the final frame to analyse. The largest value it can take  should be equal to the number of files produced during the network connectivity analysis. In our example 11.
4. **Nmax**: Loops can be formed at the most by this number of water-molecules (set it to 25).

The analysis will create the file **Loops.txt** with columns:

> 1.- frame
>
> 2.- The number of water molecules ($l$) connected and forming a loop 
>
> 3.- The next columns represent the molecule-id of all water molecules connected (in order) in the loop.

Also the file **attempts.txt** will be created, with columns:

>1.- frame
>
>2.- The number of successful attempts to find a loop.
>
>3.- The number of unsuccessful attempts to find a loop
>
>4.- An approximate of the number of loops



## Use of Kymoknot

The bash script (**bash.kymoKnot.sh**) contains the instructions to execute **dataFormatKymoknot.c++**. This program reads the particleIDs of oxygens in the loops found with the mathematica notebook (stored in *Loops.txt*), and it also reads the snapshots from simulations in the folder *../2_Loops/2_splitTraj/*. In this way it extracts the position of oxygens after reconstruction of loops through the PBC.

 **IMPORTANT:** the variables $t_{i}$, $t_{f}$ and $d_{freq}$ of this section (lines 23-27 in the bash script) should be the same as the ones used in the mathematica notebook. This analysis will create a file per frame (called **centreline_l25_tX**, with X corresponding to the time-frame) in a format compatible with kymonot: 

> N
>
> x<sub>0</sub> y<sub>0</sub> z<sub>0</sub>
>
> x<sub>1</sub> y<sub>1</sub> z<sub>1</sub>
>
> ...
>
> x<sub>N-1</sub> y<sub>N-1</sub> z<sub>N-1</sub>

where N is the length of the coordinate sets.  If the input files contain coordinate of rings, the edges x<sub>0</sub> y<sub>0</sub> z<sub>0</sub> and x<sub>N-1</sub> y<sub>N-1</sub> z<sub>N-1</sub> must coincide. Several rings (of the same length l=25) are given one after the other in the same input file. Then, we use **Kymoknot** to find knots, which will produce the file: **BU__centreline_l25_t11.dat**.

Then, we use AWK to check if the file previously created by KymoKnot contains any knot. If this is the case, it creates the file **kknotAnalysis.txt** which is a copy of *BU__centreline_l25_t11.dat*, but it only contains information of the loops with knots: 

> 1.- Ring number: starting from 0 and corresponding to the rings in *Loops.txt*
>
> 2.- Adet_1: the Alexander determinant evaluated at -1. This is equal to 1 for the unknot. 
>
> 3.- Adet_2: the Alexander determiant evaluated at . This is equal to UN for the unknot.
>
> 4.- start: the molecule in the loop where the knot starts.
>
> 5.- end: the water-molecule in the loop where the knot ends.
>
> 6.- length: number of molecules in the shortest knotted arc.

We also create **idFoundKnots.txt**, in which we use the *ring number* (from *kknotAnalysis.txt*) in order to extract the moleculeID (found in *Loops.txt*) of particles in the knotted loop.

Then, we use **awk.printConfig.sh** to extract the 3D configuration (from *centreline_l25_tX.dat*) of the knotted loops. This information is stored in a single file **ConfKnots.txt**.

Finally, if at least one knot was found, we remove the file *BU__centreline_l25_t11.dat* with the original analysis from kymoknot. Otherwise, we keep the file as a proof that the analysis was performed. 

Regardless if any knot was found, we erase the file *centreline_l25_tX* containing the coordinates of all the loops. This is just to save space.
