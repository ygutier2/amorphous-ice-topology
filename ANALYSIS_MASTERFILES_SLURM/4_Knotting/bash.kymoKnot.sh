#!/bin/bash

###################################################
################################################### 
#Executables kymoknot
KknotLinear="/storage/datastore-personal/ygutier2/SoftwareMD/KymoKnot-master/./KymoKnot_linear.x"
KknotRing="/storage/datastore-personal/ygutier2/SoftwareMD/KymoKnot-master/./KymoKnot_ring.x"
KknotClose="/storage/datastore-personal/ygutier2/SoftwareMD/KymoKnot-master/./K_close.x"

lmax=25

#IMPORTANT: the parameters here should be the same as the ones in the mathematica script:
#Initial frame to analyse
ti=11
#Final frame to analyse
tf=11
#Frequency step of frames
dfreq=1

######################################################
#KymoKnot analysis to find the loops that are knotted
######################################################
echo "KYMOKNOT ANALYSIS"
  
  for t in $( seq ${ti} ${dfreq} ${tf})
  do    
        echo "Computing Alexander determinant for l=${lmax}"
        ${KknotRing} centreline_l${lmax}_t${t}.dat
  done

##################################################
#EXTRACT RELEVANT INFORMATION OF the knotted path
##################################################
#File with the analysis obtained from Kymo-Knot 
data_file="BU__centreline_l${lmax}_t11.dat"

#File to store the output from KymoKnot (only when a knot was found)
data_knots="kknotAnalysis.txt"

#File to store the moleculeID of particles in a knot
idKnots="idFoundKnots.txt"

# Check if at least one knot was found
#Checking if the file exists
if [ -f "${data_file}" ]; then
  flag=$(awk '(NR >= 4 && $4 != "UN"){bandera=1} END {print bandera}' ${data_file})
fi

#If the file does not exist set flag to zero
if [ ! -f "${data_file}" ]; then
    flag=0
fi


#When knots are found:
if [ "${flag}" -eq 1 ]; then

    #Command to store the molecule-ID of particles in a knot
    awk 'NR >= 4 && $4 != "UN" { print $0 }' ${data_file} > ${data_knots}
    
    #Command to extract the moleculeID of particles forming the knots.
    #First get the ringID from the first column of "${data_knots}". Note that ringID starts from 0.
    #The ringID is equal to the line_number-1, in file Loops.txt.
    awk 'NR == FNR { ringID[$1+1]; next } FNR in ringID { print }' ${data_knots} Loops.txt > ${idKnots}

    #Command to store the 3D configuration of knots    
    awk -v lm="${lmax}" '{
        #Ring number (first column from ${data_knots})
        rn=$1;
        
        #starting line (this is the line number from centreline_l${lmax}_t11.dat, corresponding to rn)
        sl=(lm+2)*rn + 1;
        
        #ending line (this is the line number from centreline_l${lmax}_t11.dat, corresponding to rn)
        el=sl+lm+1;               
        
        cmd = "./awk.printConfig.sh " sl " " el;
        print cmd;
        system(cmd);
            
    }' "${data_knots}"
    
    #If knots were found, remove the original analysis from kymoknot 
    #rm ${data_file}

    #Remove the 3D paths of loops
    #rm centreline_l${lmax}_t11.dat
fi

echo "Finish of the analysis"
