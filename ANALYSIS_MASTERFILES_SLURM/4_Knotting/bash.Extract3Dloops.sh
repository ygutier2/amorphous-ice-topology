#!/bin/bash

###################################################
###################################################
lmax=25

#IMPORTANT: the parameters here should be the same as the ones in the mathematica script:
#Initial frame to analyse
ti=11
#Final frame to analyse
tf=11
#Frequency step of frames
dfreq=1

#######################
#Run mathematica script
#######################
#IMPORTANT: before running the mathematica notebook "NminLoop_v2.m" you need to replace the value of the variables:
#a) Ntot: the total number of particles in the simulation can be found inside the trajectory file:
#      "../1_network_connectivity/networkConn/REP1/traj.lammpstrj"
#b) tf: This variable should be equal to the number of files produced during the network connectivity analysis.
#      "../1_network_connectivity/networkConn/REP1/network_connectivity_frameX.dat"
#c) Nmax: Loops can be formed at the most by this number of water-molecules. In the paper Nature Physics volume 18, pages 1248–1253 (2022) this is set to Nmax=13.

#The mathemathica scripts reads the data from the network connectivity files.
#These files contain information about which water-molecule is connected with any other water-molecule.

#First you need to run the Mathematica script. The script looks if there is a loop made by exactly 25 molecules passing through each of the vertices.
#The mathematica script will create a file "Loops.txt" with columns:
#1.- frame
#2.- The number of water molecules (l=25) connected in a loop that passes through molecule (r1)
#3.- The next columns represent the moleculeIDs of all molecules connected (in order, from r1).


#Also the file "attempts.txt" will be created. This contains
#1.- frame
#2.- The number of successful attempts to find a loop
#3.- The number of unsuccessful attempts, either because:
#    a) the chosen vertex was not part of the network diagram, meaning that it was not connected to any other water molecule.
#    b) the chosen vertex was not part of any loop.
#4.- The total number of loops (passing through any of the vertices).


############
#Data format
############
#Remove lines that contain the string "VertexList"
sed -i '/VertexList/d' Loops.txt

#Remove new lines found after the special character \
sed -i '/\\$/{N;s/\n//}' Loops.txt
sed -i '/\\$/{N;s/\n//}' Loops.txt
sed -i '/\\$/{N;s/\n//}' Loops.txt

#Remove the \ special characters
sed -i 's/\\//g' Loops.txt

#Remove all the ",{} characters
sed -i 's/"//g' Loops.txt
sed -i 's/{//g' Loops.txt
sed -i 's/}//g' Loops.txt
sed -i 's/,//g' Loops.txt

#Remove " from attempts file
sed -i 's/"//g' attempts.txt


#############################################################
#Here we compute the position of all Oxygens forming loops
#############################################################
echo "FORMAT COMPATIBLE WITH KYMOKNOT"
 #c++ dataFormatKymoknot.c++ -o dataFormatKymoknot.out -lm
 
 #1.- Path to the input data
 datapath="../2_Loops/2_splitTraj/"

 #2.- Output path
 outputpath="./"

 #3.- Input file with configuration information (without timestep)
 rfile="Conf"

 # READ Variables from the file Loops.txt#
 #############################################
 awk -v dp="${datapath}" -v op="${outputpath}" -v rf="${rfile}" '{
   arguments = dp" "op" "rf;
   
   for (i = 1; i <= NF; i++) {
     col[i] = $i;
     arguments = arguments" "col[i];     
   }
   
   #print "Arguments:", arguments;
   # Execute the C++ program and pass the concatenated values as a single argument
   cmd = "./dataFormatKymoknot.out " arguments;
   system(cmd);
 }' Loops.txt
