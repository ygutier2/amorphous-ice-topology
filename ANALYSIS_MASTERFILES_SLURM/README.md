# Before performing analysis

You have to give execution permission to every single bash script and compile c++ programs, before starting this analysis. This can be achieved by running the bash script **bashExecPerm.sh**

**IMPORTANT:** Here we provide the SLURM scripts used to analyse results from simulations during compression, when the pressure was varied in the range $P\in$ [0.01, 2.0] GPa, and with a step of $\vert \Delta P \rvert=0.01GPa$ (giving a total of 200 different pressures). We assume that the data is organized with 200 folders named *Pressure_X* (with X/10000 the value of the pressure in GPa) each folder containing a file called **fort.100** with 11 thermally equilibrated configurations. The analysis also assumes that we deal with a system made of $M=512$ water molecules.

In the file **trajectories_100K_Run1_Compression.zip** you can find the 200 configurations in the format required by the analysis. Please UNZIP the file and copy its content in the folder above (where the six different pressures can be found).

For users without previous experience using SLURM, we recommend to start with the analysis for only six pressures described in *ANALYSIS_MASTERFILES*, so that they become familiarised with the different bits of the analysis. 


# Analysis

If you meet all the requirements (mentioned in the home address of this repository), then you simply have to execute the SLURM scripts found in this folder. These scripts are written assuming that the user has access to a cluster with 64 CPUs available and for a time in between 1 hour and 7 days (depending on the part of the analysis). The scripst MUST be run in order, starting with *slurm.parallel.part1.sh*, and waiting for each part to finish analysing the 200 pressures before running the next part.

Each of the slurm scripts will automatically execute one of the Main Bash scripts briefly described below, for 64 different pressures at the same time. A more detailed explanation of the analysis performed by the Main Bash scripts can be found in the README files inside each of the three folder located here (*1\_network\_connectivity*, *2\_Loops*, *3\_Linking*). 

1. **bash.analysis1.networkconnectivity.sh**. This script copies the folder *1\_network\_connectivity* into each of the pressure folders and then it will execute the network connectivity analysis. It creates:
	* 1.1) **nc\_vs\_t.dat**.
	* 1.2) **traj.lammpstrj** 
	* 1.3) **network\_connectivity\_frameX.dat** (with X representing the corresponding frame).
	* 1.4) **frequency_frameX.txt** (with X representing the corresponding frame).
	* 1.5) **Valence/frequency_frameX.txt** (with X representing the corresponding frame).
	* 1.6) **Distributions/distprobAngleX** (with X representing the corresponding frame).
	* 1.7) **Distributions/distprobDistX**  (with X representing the corresponding frame).

2. **bash.analysis2.loops.mathematica.sh**. This script copies the folder *2\_Loops* into each of the pressure folders and then it executes the mathematica script *NminLoop.m* to find all the loops formed by water molecules. By default the analysis is performed only at the last frame (11) from simulations. The Mathematica script is executed by using the Mathematica kernel and not the usual graphical interface. When correctly executed, the mathematica script will create two files: 
	* 2.1) **Loops.txt**
	* 2.2) **attempts.txt**.

3. **bash.analysis3.loops.fractalDimension.sh**. This script moves the terminal into *2\_Loops* that can be found inside each of the pressure folders. Then it executes *bash\_NumberOfMinLoops.sh* to find all the loops. By default the analysis is performed only on the last frame (11). It creates:
	* 3.1) *1\_loopsFound* folder, here it stores **attempts.txt** and **NLoopsApprox2.txt** created in the following point.
	* 3.2) Runs *remove\_repeated\_loops.out* and it creates **Loops\_cleaned.txt** with unrepeated loops and **NLoopsApprox2.txt**.
	* 3.3) Splits original trajectory files into 11 different frames and store them in: *2\_splitTraj*
	* 3.4) Runs *fractalD\_ring\_v2.out* to create **fractalDimension2.dat**.
	* 3.5) computes histogram: **distprob**.
	* 3.6) computes **avel\_Rg.dat**.
	* 3.7) computes **NLoops.txt**, from which we compute **aveNLoops.dat**.
	* 3.8) Computes **average\_Rg\_atfixed\_l.dat**

4. **bash.analysis4.linking.IDsV2.sh**. This script copies the folder *3\_Linking* into each of the pressure folders and then it executes *bash\_DumpLoops.sh* that it extracts the particleID of all oxygens forming the loops. It creates:
	* 4.1) A file per time-frame called **LoopsParticleIDs\_tX** (with X the time-frame).

5. **bash.analysis5.linkingV2.sh**. This script moves the terminal into *3\_Linking* that can be found inside each of the pressure folders. Then it executes *bash\_computeLk.sh* to compute the linking number between all the pairs of loops previously found by our analysis. It creates:
	* 5.1) **Linking\_vs\_time.dat**
	* 5.2) **LinkingMatrix.dat**
	* 5.3) Two folders: *LinkedIDs* and *linkedRepeatedIDs*

6. **bash.analysis6.knotting.sh**. This script moves the terminal into *4\_Linking* that can be found inside each of the pressure folders. Then it executes the mathematica script *NminLoop_v2.m* to find loops formed by exactly 25 water molecules (at the most 500 000 loops passing through each vertex). By default the analysis is performed only at the last frame (11) from simulations. The Mathematica script is executed by using the Mathematica kernel and not the usual graphical interface. Instructions on how to perform the analysis through the graphical interface are found below. When correctly executed, the mathematica script will create two files: 
	* 6.1) **Loops.txt**
	* 6.2) **attempts.txt**.

Then, the script bash.Extract3Dloops.sh is executed to extract the 3D-configurations of loops; followed by the *bash.kymoKnot.sh* script, which looks for knotted configurations of the loops. More information is found in the README file inside folder *4\_Linking*.

* 6.3) **centreline_l25_tX.dat**
* 6.4) **BU__centreline_l25_tX.dat**
* 6.5) **kknotAnalysis.txt**
* 6.6) **idFoundKnots.txt**
* 6.7) **ConfKnots.txt**
