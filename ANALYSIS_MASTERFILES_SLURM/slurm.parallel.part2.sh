#!/bin/bash
#
#SBATCH --job-name=water.analysis.comp.part2.T100K
#SBATCH --partition=long
#SBATCH --time=168:00:00
#SBATCH --mem-per-cpu=2048
#This means run 200 single CPU jobs in batches of 64
#SBATCH --array=1-200%64

#The pressure to analyse during compression: pi=100, dp=100, pf=20000. A total of 200.
#NOTE: when resuming from a certain pressure, for instance =14700, you just have to set #SBATCH --array=147-200%32
dp=100
p=$(($dp*${SLURM_ARRAY_TASK_ID}))

#Print date and time when the analysis started
pdt1=$(date '+%d/%m/%Y %H:%M:%S');
echo "Starting time: $pdt1"

#The taskID:
echo "Hello from SLURM task: ${SLURM_ARRAY_TASK_ID}"

#Do the analysis  
echo "Mathematica notebook slurm"
./bash.analysis2.loops.mathematica.sh ${p}
echo "Finish Mathematica notebook slurm"

#Print date and time when the analysis finished
pdt2=$(date '+%d/%m/%Y %H:%M:%S');
echo "Ending time: $pdt2"

mv slurm-*_${SLURM_ARRAY_TASK_ID}.out outputSlurm2/
