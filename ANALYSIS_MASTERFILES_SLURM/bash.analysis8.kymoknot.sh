#!/bin/bash
  #What is the pressure you want to analyse:
  p="$1"
  
  #Current working directory
  cwd=$(pwd)
  
  
  #################################
  ## ANALYSIS DURING COMPRESSION ##
  #################################
  echo "Looking for knots during compression at P=${p}"
      
  #move terminal to the analysis folder
  cd ../Pressure_${p}/
  cd 4_Knotting/
      
  #Run bash to look for knots
  ./bash.kymoKnot.sh
      
  #Move back to MASTER folder
  cd "${cwd}"
