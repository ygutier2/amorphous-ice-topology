#include<vector>     /*Library for using vector insted of arrays*/
#include<iostream>   /*Basic functions*/   
#include <cmath>     /*Define some fundamental mathematical operations*/
#include<stdlib.h>   /*Useful for random numbers*/
#include <sstream>
#include <fstream>   /*To read and write into files*/
#include <limits>
#include <stdio.h>   /*Basic functions in particular "sprintf"*/
#include <iomanip>      // std::setprecision
#include <algorithm> //max element


using namespace std;

#define pi 3.141592653589793238462643383279502884197169399375

#define SMALL 1e-20


int main(int argc, char *argv[])
{
    /**********************************************/
    /*Pass the following arguments to the programm*/
    /**********************************************/
    /*To get help screen of the program:*/
    if(argc<2 || argc>2)
    {
        cout << "One argument is required by the programm:"  << endl;
        cout << "1.- Frame"                                  << endl;
    }


if(argc==2)
{   
           
    //cout << "ok" << endl;
    int frame;
    frame = atoi(argv[1]);    
    
    //File contains: (1) frame, (2) l; (3) Rg; (4)-(4+l-1)Molecule-id
    int t;
    int l;
    int molid;
    double Rg;
    
    //Define vectors connecting different number of water-molecules in a loop.
    vector< vector<int> > ml03;
    vector< vector<int> > ml04;
    vector< vector<int> > ml05;
    vector< vector<int> > ml06;
    vector< vector<int> > ml07;
    vector< vector<int> > ml08;
    vector< vector<int> > ml09;
    vector< vector<int> > ml10;
    vector< vector<int> > ml11;
    vector< vector<int> > ml12;
    vector< vector<int> > ml13;
    vector< vector<int> > ml14;
    vector< vector<int> > ml15;
    vector< vector<int> > ml16;
    vector< vector<int> > ml17;
    vector< vector<int> > ml18;
    vector< vector<int> > ml19;
    vector< vector<int> > ml20;
                          
    //Total number of unrepeated loops at a fixed timestep
    int Ntot=0;
    
    //Number of rings of different size [3,20] at fixed timestep
    vector<int> nloops_persize;    

    /*********************/
    /**Reads input files**/
    /*********************/
    ifstream read("fractalDimension2.dat");
 
    if(!read){cout << "Error while opening data file!"<<endl;}
    if (read.is_open())    
    {                        
        string line;
        string dummy;                

        //Loop to read the file, line by line into a string.
        while (getline(read, line))            
        {                            
            //To split the line into its corresponding values
            istringstream is(line);                                       
            is >> t >> l >> Rg;
            
            if(t==frame){
                //cout << t << " " << l;                
                vector<int> listID;
                for(int j=0; j<l; j++){
                   is >> molid;
                   
                   //Store the molecule-id in the corresponding vectors
                   listID.push_back(molid);
                   //cout << " " << listID[j];
                }
                //cout << endl;                
                
                //Push the previous version in 2D vectors that contain the molecule-ids of all the loops with the same size.
                if(l==3){ml03.push_back(listID);}
                if(l==4){ml04.push_back(listID);}
                if(l==5){ml05.push_back(listID);}
                if(l==6){ml06.push_back(listID);}
                if(l==7){ml07.push_back(listID);}
                if(l==8){ml08.push_back(listID);}
                if(l==9){ml09.push_back(listID);}
                if(l==10){ml10.push_back(listID);}
                if(l==11){ml11.push_back(listID);}
                if(l==12){ml12.push_back(listID);}
                if(l==13){ml13.push_back(listID);}
                if(l==14){ml14.push_back(listID);}
                if(l==15){ml15.push_back(listID);}
                if(l==16){ml16.push_back(listID);}
                if(l==17){ml17.push_back(listID);}
                if(l==18){ml18.push_back(listID);}
                if(l==19){ml19.push_back(listID);}
                if(l==20){ml20.push_back(listID);}
            }
        }
    }
    read.close();
    
    //CHECK
    /*
    int Nrows=ml06.size();
    for(int r=0; r<Nrows; r++){
        cout << frame << " 6 ";
        for(int j=0; j<6; j++){
            cout << ml06[r][j] << " ";
        }
        cout << endl;
    }
    */
            
    //Number of rows in the 2D-vector is the number of loops of that size
    int Nl03=ml03.size();
    nloops_persize.push_back(Nl03);
    Ntot+=Nl03;
    
    int Nl04=ml04.size();
    nloops_persize.push_back(Nl04);
    Ntot+=Nl04;    
      
    int Nl05=ml05.size();
    nloops_persize.push_back(Nl05);
    Ntot+=Nl05;
        
    int Nl06=ml06.size();
    nloops_persize.push_back(Nl06);
    Ntot+=Nl06;
    
    int Nl07=ml07.size();
    nloops_persize.push_back(Nl07);
    Ntot+=Nl07;

    int Nl08=ml08.size();
    nloops_persize.push_back(Nl08);
    Ntot+=Nl08;

    int Nl09=ml09.size();
    nloops_persize.push_back(Nl09);
    Ntot+=Nl09;   
    
    int Nl10=ml10.size();
    nloops_persize.push_back(Nl10);
    Ntot+=Nl10;
    
    int Nl11=ml11.size();
    nloops_persize.push_back(Nl11);
    Ntot+=Nl11;
    
    int Nl12=ml12.size();
    nloops_persize.push_back(Nl12);
    Ntot+=Nl12;
    
    int Nl13=ml13.size();
    nloops_persize.push_back(Nl13);
    Ntot+=Nl13;

    int Nl14=ml14.size();
    nloops_persize.push_back(Nl14);
    Ntot+=Nl14;
    
    int Nl15=ml15.size();
    nloops_persize.push_back(Nl15);
    Ntot+=Nl15;

    int Nl16=ml16.size();
    nloops_persize.push_back(Nl16);
    Ntot+=Nl16;

    int Nl17=ml17.size();
    nloops_persize.push_back(Nl17);
    Ntot+=Nl17;

    int Nl18=ml18.size();
    nloops_persize.push_back(Nl18);
    Ntot+=Nl18;

    int Nl19=ml19.size();
    nloops_persize.push_back(Nl19);
    Ntot+=Nl19;

    int Nl20=ml20.size();
    nloops_persize.push_back(Nl20);
    Ntot+=Nl20;
        
           
    /****************/
    /*Print results:*/
    /****************/
    stringstream writeFile2;
    ofstream write2;
    writeFile2 <<"NLoops.txt";
    write2.open(writeFile2.str().c_str(), std::ios_base::app);

    //set precision and the number of decimals to be printed always
    write2.precision(5);
    write2.setf(ios::fixed);
    write2.setf(ios::showpoint);
    
    write2 << frame << " " << Ntot;
    for(int j=0; j<nloops_persize.size(); j++){
        write2 << " " << nloops_persize[j];
    }
    write2 << endl;
    
}





return 0;
}
