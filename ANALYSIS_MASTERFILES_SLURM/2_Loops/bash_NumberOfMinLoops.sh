#!/bin/bash
#System details
  

#######################
#Run mathematica script
#######################
#IMPORTANT: before running the mathematica notebook "NminLoop_v2.m" you need to replace the value of the variables:
#a) Ntot: the total number of particles in the simulation can be found inside the trajectory file:
#      "../1_network_connectivity/networkConn/REP1/traj.lammpstrj"
#b) tf: This variable should be equal to the number of files produced during the network connectivity analysis.
#      "../1_network_connectivity/networkConn/REP1/network_connectivity_frameX.dat"
#c) Nmax: Loops can be formed at the most by this number of water-molecules. In the paper Nature Physics volume 18, pages 1248–1253 (2022) this is set to Nmax=13.

#The mathemathica scripts reads the data from the network connectivity files.
#These files contain information about which water-molecule is connected with any other water-molecule.

#First you need to run the Mathematica script. The script looks if there is a loop made by 1-13 molecules passing through each of the vertices.
#For instance, if a loop passing through molecule-1 is made by (1,2,6,28,39 y 100). This means that when the algoritm looks for
#a loop passing through vertex 2, there is the possibility that this loop contains the same molecules as the previous one (2,6,28,39,100 y 1)
#In this case, the number of loops would be over counted.

#The mathematica script will create a file "Loops.txt" with columns:
#1.- frame
#2.- The number of water molecules (l) connected in a loop that passes through molecule (r1)
#3.- The next columns represent the moleculeIDs of all molecules connected (in order, from r1).


#Also the file "attempts.txt" will be created. This contains
#1.- frame
#2.- The number of successful attempts to find a loop
#3.- The number of unsuccessful attempts, either because:
#    a) the chosen vertex was not part of the network diagram, meaning that it was not connected to any other water molecule.
#    b) the chosen vertex was not part of any loop.
#4.- The total number of loops (passing through any of the vertices).

##########################################################################################
#IMPORTANT: the parameters here should be the same as the ones in the mathematica script:#
##########################################################################################
ti=11
tf=11
dfreq=1


############
#Data format
############
#Remove lines that contain the string "VertexList"
sed -i '/VertexList/d' Loops.txt

#Remove new lines found after the special character \
sed -i '/\\$/{N;s/\n//}' Loops.txt

#Remove the \ special characters
sed -i 's/\\//g' Loops.txt

#Remove all the ",{} characters
sed -i 's/"//g' Loops.txt
sed -i 's/{//g' Loops.txt
sed -i 's/}//g' Loops.txt
sed -i 's/,//g' Loops.txt

#Remove " from attempts file
sed -i 's/"//g' attempts.txt


##################################################################
#Use AWK script to remove repeated minimum loops at a fixed time#
##################################################################
echo "REMOVING REPEATED LOOPS"
./awk.remove_repeated_lopps.sh

#This program will create the file "Loops_cleaned.txt" is a copy of the original file but we have removed the repeated minimum loops

mkdir 1_loopsFound
mv attempts.txt      1_loopsFound


###################################################
#Split trajectory into single files per time-frame#
###################################################
echo "Splitting configuration files"
#Compile the programm:
#c++ -std=c++11 splitTraj.cpp -o splitTraj
  
#1.- Path to the input data
datapath="../"
      
#2.- Output path
outputpath="./"
      
#3.- Input file with atoms information
rfile=$(basename ../f*)
      
#The total number of files inside the DUMP folder
nf=$(ls -lorth ${datapath}${rfile} | wc -l)
echo "The total number of data files is:  ${nf}"
    
if [ "$nf" -gt 0 ]
then              
 ./splitTraj ${datapath} ${outputpath} ${rfile}
fi

mkdir 2_splitTraj
mv Conf* 2_splitTraj/


#############################################################################################################################################################
#Compute the Rg of the Loops. Then, choose the water-molecules with Rg<L/3 sigma and the ones with Re2e<rc (where rc is the oxygen-oxygen threshold distance)
#############################################################################################################################################################
echo "COMPUTING Rg and fractalD analysis"

 #1.- Path to the input data
 datapath="2_splitTraj/"

 #2.- Output path
 outputpath="./"

 #3.- Input file with configuration information (without timestep)
 rfile="Conf"

 # READ Variables from the file Loops_cleaned.txt#
 #################################################
 awk -v dp="${datapath}" -v op="${outputpath}" -v rf="${rfile}" '{
   arguments = dp" "op" "rf;
   
   for (i = 1; i <= NF; i++) {
     col[i] = $i;
     arguments = arguments" "col[i];     
   }
   
   #print "Arguments:", arguments;
   # Execute the C++ program and pass the concatenated values as a single argument
   cmd = "./fractalD_ring_v2.out " arguments;
   system(cmd);
 }' Loops_cleaned.txt
 
 mv Loops_cleaned.txt 1_loopsFound
 #rm fractalD_ring_v2.out
  
 #This programm will create a file called (fractalDimension2.dat) with the following columns:
 #1.-Timestep
 #2.-l: number of water forming a loop
 #3.-Rg: radius of gyration of the loop
 #>4.- the molecule-id of the l molecules in the loop
 
 
##########################################
#Compute the histogram of the loops length
##########################################
echo "Computing histogram"
   
# minimum value of x axis
m="0"
  
# maximum value of x axis
M="13"
  
# number of bins
b="13"

# output file for plotting
fout="distprob"
  
# input file
fin="fractalDimension2.dat"
  
gawk -f awk.histbin ${m} ${M} ${b} ${fout} ${fin}


############################################################################
#Compute the average length and radius of gyration of REAL UNREPEADTED LOOPS
############################################################################
#The avergae is performed over all timesteps
awk '{aveL+=$2; aveRg+=$3; i++; deltaL=$2-avgL; avgL+=deltaL/NR; meanL2+=deltaL*($2-avgL); deltaR=$3-avgR; avgR+=deltaR/NR; meanR2+=deltaR*($3-avgR);} END { print aveL/i,sqrt(meanL2/NR), aveRg/i,sqrt(meanR2/NR); }' fractalDimension2.dat > avel_Rg.dat


#################################################################
#Compute the number of REAL UNREPEADTED LOOPS as function of time
#################################################################
#The format of this file is very similar to "remove_repeated_loops.cpp".

echo "Computing Loops distribution"
#IMPORTANT: the parameters here should be the same as the ones inthe mathematica script.
#These parameters: ti, dfreq, tf were set above

#c++ loops_distribution.cpp -o loops_distribution.out -lm

for tt in $( seq ${ti} ${dfreq} ${tf} )
do 
  ./loops_distribution.out ${tt}
done

#rm loops_distribution.out

#This program will create one files:

#NLoops.txt with the following columns:
#1.- timestep
#2.- Total number of unrepeated loops (regardless their size).
#3.- From column 3 onwards it is shown the number of loops with sizes: 3,4,5,6,7,...,20

awk '{aveNmin+=$2; i++; deltaN=$2-avgN; avgN+=deltaN/NR; meanN2+=deltaN*($2-avgN);} END { print aveNmin/i,sqrt(meanN2/NR); }' NLoops.txt > aveNLoops.dat


################################################################
#Compute average of R and Rg at the same l from the scatter plot
################################################################
echo "Computing average of Rg at same l"
 #chmod a+x awk.ave_at_same_l.sh
 ./awk.ave_at_same_l.sh

 echo "Finish of the analysis"      
 
#Check visually with gnuplot: for l_vs_Rg     plot "fractalDimension2.dat" u 2:3, "average_Rg_atfixed_l.dat" u 1:2:3 w yerrorbars lw 3 

echo "Finish of the analysis"
