#!/bin/bash
  #What is the pressure you want to analyse:
  p="$1"
  
  #Current working directory
  cwd=$(pwd)
  
  
  ##########################################
  ## ANALYSIS: RUN THE MATHEMATICA SCRIPT ##
  ##########################################
  echo "Running Mathematica script at P=${p}"
      
  #move terminal to the analysis folder and remove previous analysis
  cd ../Pressure_${p}/
  if [ -e "2_Loops" ]; then
     rm -r 2_Loops
  fi
                        
  #Copy folder to the corresponding directory
  cp -r "${cwd}/2_Loops" .
  cd 2_Loops

  #Run mathematica script in the terminal
  math -script NminLoop_v2.m
      
  #Move back to MASTER folder
  cd "${cwd}"
