#!/bin/bash
  #What is the pressure you want to analyse:
  p="$1"
  
  #Current working directory
  cwd=$(pwd)
  
  
  #################################
  ## ANALYSIS DURING COMPRESSION ##
  #################################
  echo "Linking during compression at P=${p}"
      
  #move terminal to the analysis folder
  cd ../Pressure_${p}/3_Linking/
                                    
  #Run bash to compute the Lk between loops
  ./bash_computeLk.sh
      
  #Move back to MASTER folder
  cd "${cwd}"
