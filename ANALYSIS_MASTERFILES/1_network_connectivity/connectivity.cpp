#include<vector>     /*Library for using vector insted of arrays*/
#include<iostream>   /*Basic functions*/   
#include <cmath>     /*Define some fundamental mathematical operations*/
#include<stdlib.h>   /*Useful for random numbers*/
#include <sstream>
#include <fstream>   /*To read and write into files*/
#include <limits>
#include <stdio.h>   /*Basic functions in particular "sprintf"*/
#include <iomanip>      // std::setprecision
#include <tuple>     //Functions can return more than one value
#include <set>
#include <algorithm>



using namespace std;

#define pi 3.141592653589793238462643383279502884197169399375
#define SMALL 1e-20

int main(int argc, char *argv[])
{
    /**********************************************/
    /*Pass the following arguments to the programm*/
    /**********************************************/
    /*To get help screen of the program: ./trajectories help*/
    if(argc<6)
    {
        cout << "Four arguments are required by the programm:"              << endl;
        cout << "1.- Path to the input data"                                << endl;
        cout << "2.- Output path"                                           << endl;
        cout << "3.- Input file (without timestep)"                         << endl;
        cout << "4.- Cutoff distance for a contact"                         << endl;
        cout << "5.- Cutoff angle for a contact"                            << endl;
    }

if(argc==6)
{   
    cout << "ok" << endl;
    /*argv[1] Directory where the data is*/
    /*argv[2] Directory where the output will be stored*/
    /*argv[3] Input file*/
    
    /*Cutoff distance between H(A)--O(B), in Angstroms*/
    float rc;
    rc = atof(argv[4]);
    
    /*Cutoff angle between O(B)O(A)-axis and H(A)O(B)-bond, in degrees*/
    float ac;
    ac = atof(argv[5]);
    
    /*Angle*/

    /*timestep*/
    int timestep;
    
    //The total number of atoms (512 water molecules. 3 particles each)
    int Natoms=1536;
    
    //Each water molecule is made by two hydrogens and one oxigen.
    //So the number of particles per molecule is 3:
    int Nppmol=3;
    
    //The number of molecules: Natoms/Nppmol
    int M=Natoms/Nppmol;
    
    //Reads this information
    vector< vector< vector<float> > > position;
    position.resize(M, vector<vector<float>>(Nppmol, vector<float>(3)));
    
    vector< vector<int> > typevec;
    typevec.resize(M, vector<int>(Nppmol));
    
    vector< vector<int> > index;
    index.resize(M, vector<int>(Nppmol));
    
    
    //To cout the same number of decimals as in the files we are reading
    std::cout << std::fixed;
    std::cout << std::setprecision(7);

    //Useful variables to read the file
    float Lx,Ly,Lz;
    float x,y,z;
    char type;
    char type1 = 'O';
    char type2 = 'H';
    
    string line;
    
    ifstream inputFile;
    char readFile[400] = {'\0'};
    sprintf(readFile, "%s%s",argv[1],argv[3]);
    inputFile.open(readFile);
    
    //Error if file not found
    if (!inputFile) {
        std::cerr << "Error: Unable to open the file." << std::endl;
        return 1;
    }
    
 
    int frame=0;
    int ln=0;
    
    //Reads the file line by line
    while (std::getline(inputFile, line)) {
        std::istringstream ss(line);
        
        //Read First line
        if(ln==0){
            ss >> line;
        }
        
        //Second line contains the simulation box size in Angstroms
        if(ln==1){
            ss >> Lx >> Ly >> Lz;
            //cout << ln << " " << Lx << " " << Ly << " " << Lz << endl;
        }
        
        //The next Natoms lines contain the positions of particles
        if (ln>1){
            //Try to extract type and position from each line
            if (ss >> x >> y >> z){
                //Molecule id
                int m = floor((double)(ln-2)/(1.0*Nppmol));
            
                position[m][(ln-2)%Nppmol][0] = x;
                position[m][(ln-2)%Nppmol][1] = y;
                position[m][(ln-2)%Nppmol][2] = z;
                
                //Particle index
                index[m][(ln-2)%Nppmol]=ln-1;
                
                //It reads one Oxigen followed by two hydrogens
                //For oxigen particles
                if((ln-2)%3==0){
                    typevec[m][(ln-2)%Nppmol] = 0;
                }

                //For hydrogen particles
                if((ln-2)%3!=0){
                    typevec[m][(ln-2)%Nppmol] = 1;
                }

                //Print index, type, xyz positions
                 //cout << index[m][(ln-2)%Nppmol] << " " << typevec[m][(ln-2)%Nppmol] << " " << x << " " << y << " " << z << endl;                
            } 
        }        
        //Line number increment
        ln++;
        
        
        //Check positions of molecules at a given frame:
        /*
        if(ln==Natoms+2){
            for(int m=0; m<M; m++){
                for(int n=0; n<Nppmol;n++){
              cout << index[m][n] << " " << m << " " << typevec[m][n] << " " << position[m][n][0] << " " << position[m][n][1] << " " << position[m][n][2] << endl;
                }
            }
        }
        */
        
        
        //After reading all the lines related to a frame:
        //1.- reset the line number.
        //2.- increase the frame count.
        //3.- Perform the analysis corresponding to that frame
        if(ln==Natoms+2){
            ln=0;
            frame++;
            //cout<< "Analysis for frame=" << frame << endl;

            //The molecule ID of the two water molecules in contact
            vector<int> WM1;
            vector<int> WM2;
            //The particle ID of the HB interaction
            vector<int> H1id;
            vector<int> O2id;
            //The angle and distance when there is a HB
            vector<float> angle;
            vector<float> distance;
                    
            //Two water molecules (A and B) are linked through HB interactions if:
            //a) The distance O(A)H(A) --- O(B) < 2.2 Angstrom
            //b) The angle H(A)O(A) --- O(B) < 30 degrees
            double hx,hy,hz,ox,oy,oz;
            double ox1,oy1,oz1;
            int idh,ido,idh1,ido1;

            int nc=0;
            double dx,dy,dz;
            for(int m=0;m<M;m++){
                //position of oxygen in the A-molecule
                ox=position[m][0][0];
                oy=position[m][0][1];
                oz=position[m][0][2];
                ido=index[m][0];

                for(int n=0; n<2; n++){
                    //Position of hydrogen in the A-molecule
                    hx=position[m][n+1][0];
                    hy=position[m][n+1][1];
                    hz=position[m][n+1][2];
                    idh=index[m][n+1];
                                        
                    //For the B molecule
                    for(int m1=0;m1<M;m1++){
                       if(m!=m1){
                           //Position of oxigen in the B-molecule
                           ox1=position[m1][0][0];
                           oy1=position[m1][0][1];
                           oz1=position[m1][0][2];
                           ido1=index[m1][0];
                           
                           //Distance between H(A)--O(B)
                           dx=hx-ox1;
                           dy=hy-oy1;
                           dz=hz-oz1;
                       
                           float dist = sqrt(dx*dx+dy*dy+dz*dz);
                       
                           if(dist<rc){
                               //Compute the Angle between the O1-O axis and the O1-H bond
                               float ux = ox-ox1;
                               float uy = oy-oy1;
                               float uz = oz-oz1;
                               float d1 =sqrt(ux*ux+uy*uy+uz*uz);
                               ux=ux/d1;
                               uy=uy/d1;
                               uz=uz/d1;
                           
                               float vx = hx-ox1;
                               float vy = hy-oy1;
                               float vz = hz-oz1;
                               float d2 =sqrt(vx*vx+vy*vy+vz*vz);
                               vx=vx/d2;
                               vy=vy/d2;
                               vz=vz/d2;
                               
                               float costheta = ux*vx +uy*vy + uz*vz;
                               float theta=acos(costheta)*180.0/M_PI;
                               
                               //cout << theta << endl;
                               
                               //There is an HB
                               if(theta<ac){
                                   //The molecule ID of the two water molecules in contact                                                 
                                   WM1.push_back(m+1);
                                   WM2.push_back(m1+1);
                                   
                                   //The particle ID of the HB interaction
                                   H1id.push_back(idh);
                                   O2id.push_back(ido1);
                                   
                                   //The angle and distance
                                   angle.push_back(theta);
                                   distance.push_back(dist);
                                   
                                   //Count contacts
                                   nc++;                             
                               }
                           }//Close if dist<rc
                       }//close if m!=m1
                  }//Close loop on B molecule                  
               }//Close loop on hydrogens of A-molecule
            }//Close loop on A-molecule
            
            
            /////////////
            // PRINT NC
            /////////////
            stringstream writeFile1;
            ofstream write1;
            writeFile1 <<"nc_vs_t.dat";
            write1.open(writeFile1.str().c_str(), std::ios_base::app);
            write1 << frame << " " << nc << endl;
            
            //////////////////////////////////////////////
            // PRINT Network connection at each frame
            //////////////////////////////////////////////
            if(WM1.size()>0){
                char writeFile2[400] = {'\0'};
                sprintf(writeFile2, "%snetwork_connectivity_frame%d.dat",argv[2],frame);
                ofstream write2;
                write2.open(writeFile2);    
        
                for(int i=0; i<WM1.size();i++){
                    write2 << WM1[i] << " " << WM2[i] << " " << H1id[i] << " " << O2id[i] << endl;
                }
                write2.close();
                write2.clear();
            }
            
            //////////////////////////////////////////////
            // PRINT angle and distance at each frame
            //////////////////////////////////////////////
            if(angle.size()>0){
                char writeFile4[400] = {'\0'};
                sprintf(writeFile4, "%sangle_dist_frame%d.dat",argv[2],frame);
                ofstream write4;
                write4.open(writeFile4);    
        
                for(int i=0; i<angle.size();i++){
                    write4 << angle[i] << " " << distance[i] << endl;
                }
                write4.close();
                write4.clear();
            }
                
            //////////////////////////////////////////////
            // PRINT VMD visualization
            //////////////////////////////////////////////
            stringstream writeFile3;
            ofstream write3;
            writeFile3 <<"traj.lammpstrj";
            write3.open(writeFile3.str().c_str(), std::ios_base::app);

            write3 << "ITEM: TIMESTEP"            << endl;
            write3 << frame                       << endl;
            write3 << "ITEM: NUMBER OF ATOMS"     << endl;
            write3 << Natoms                      << endl;
            write3 << "ITEM: BOX BOUNDS pp pp pp" << endl;
            write3 << -Lx/2.0 << " " << Lx/2.0    << endl;
            write3 << -Ly/2.0 << " " << Ly/2.0    << endl;
            write3 << -Lz/2.0 << " " << Lz/2.0    << endl;
            write3 << "ITEM: ATOMS id mol type x y z" << endl;
            for(int m=0; m<M; m++){
              for(int n=0; n<Nppmol;n++){
                int id=index[m][n];
                int mol = floor((float)(id-1)/(1.0*Nppmol));
                char tipo;
                if(typevec[m][n]==0){tipo='O';}
                if(typevec[m][n]==1){tipo='H';}
                //int tipo;
                //tipo=typevec[m][n];
            write3 << id << " " << mol+1 << " " << tipo << " " << position[m][n][0] << " " << position[m][n][1] << " " << position[m][n][2] << endl;
              }
            }
            
            
            
        
            
        }//Close if statement after reading all the data from a single frame
    }//Close while loop after reading all frames
    
    //cout << "last line number: " << ln << endl;

    inputFile.close(); // Close the file after reading
             
}


return 0;
}
