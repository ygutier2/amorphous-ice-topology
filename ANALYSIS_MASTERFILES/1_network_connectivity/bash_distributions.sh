#!/bin/bash
###############################################################################
#At a fixed timestep check the number of contacts of molecule m, with the rest#
###############################################################################
  ti=1
  tf=11
  dfreq=1
  
  mkdir Valence
  
  for tt in $( seq ${ti} ${dfreq} ${tf} )
  do 
    ./occurrences.awk network_connectivity_frame${tt}.dat >> foccurrences_frame${tt}.txt
    
    #check the frequencies of each valence
    awk '{count[$2]++} END {for (num in count) print num, count[num]}' foccurrences_frame${tt}.txt >> frequency_frame${tt}.txt
    
    mv foccurrences_frame${tt}.txt Valence/
    mv frequency_frame${tt}.txt    Valence/
  done
 
############################################################
#Compute the distribution of angles when there is a contact#
############################################################
echo "Computing histogram angle"

mkdir Distributions
  
# minimum value of theta axis
m="0"
  
# maximum value of theta axis
M="30"
  
# number of bins
b="30"

  for tt in $( seq ${ti} ${dfreq} ${tf} )
  do 
    # output file for plotting
    fout="distprobAngle${tt}"
  
    # input file with angle and distance
    fin="angle_dist_frame${tt}.dat"
  
    gawk -f awk.hist_thetaHB ${m} ${M} ${b} ${fout} ${fin}
    
    mv ${fout} Distributions/
  done

###############################################################
#Compute the distribution of distances when there is a contact#
###############################################################
echo "Computing histogram distances"
  
# minimum value of distance axis
m="0"
  
# maximum value of distance axis
M="2.2"
  
# number of bins
b="22"

  for tt in $( seq ${ti} ${dfreq} ${tf} )
  do 
    # output file for plotting
    fout="distprobDist${tt}"
  
    # input file
    fin="angle_dist_frame${tt}.dat"
  
    gawk -f awk.hist_distHB ${m} ${M} ${b} ${fout} ${fin}
    
    mv ${fout} Distributions/
    mv ${fin}  Distributions/
  done




echo "Finish of the analysis"
