#!/bin/bash

   echo "Executing permission:"   
   chmod a+x bash.analysis1.networkconnectivity.sh
   chmod a+x bash.analysis2.loops.mathematica.sh
   chmod a+x bash.analysis3.loops.fractalDimension.sh
   chmod a+x bash.analysis4.linking.IDsV2.sh
   chmod a+x bash.analysis5.linkingV2.sh


   echo "Inside 1_network_connectivity"
   cd 1_network_connectivity
   c++ -std=c++11 connectivity.cpp -o connectivity
	  chmod a+x bash_distributions.sh
   chmod a+x occurrences.awk
	  chmod a+x awk.hist_distHB
	  chmod a+x awk.hist_thetaHB
	  cd ../
	  
	  echo "Inside 2_Loops"
	  cd 2_Loops/
	  chmod a+x bash_NumberOfMinLoops.sh
	  chmod a+x awk.remove_repeated_lopps.sh
   c++ -std=c++11 splitTraj.cpp -o splitTraj
   c++ fractalD_ring_v2.cpp -o fractalD_ring_v2.out -lm
   c++ loops_distribution.cpp -o loops_distribution.out -lm
   chmod a+x awk.ave_at_same_l.sh
   cd ../
   
   echo "Inside 3_Linking"
   cd 3_Linking/
	  chmod a+x bash_DumpLoops.sh
	  chmod a+x bash_computeLk.sh
	  c++ LoopsIDs.cpp -o LoopsIDs.out -lm
	  c++ -std=c++11 ComputeLinking_water.c++ -o ComputeLinking_water
	  cd ../
	  
	  echo "Inside 4_Knotting"
	  cd 4_Knotting/
	  chmod a+x bash.Extract3Dloops.sh
	  chmod a+x bash.kymoKnot.sh
	  c++ dataFormatKymoknot.c++ -o dataFormatKymoknot.out -lm
   cd ../

