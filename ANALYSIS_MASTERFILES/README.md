# Before performing analysis

You have to give execution permission to every single bash script and compile c++ programs before starting the analysis. This can be achieved by running the bash script **bashExecPerm.sh**

**IMPORTANT:** The analysis is computational expensive. Computing the network connectivity, finding all the loops and the linking between them, for *only one configuration (time-frame)* at each of the six pressures provided in this repository can take up-to 15 hours in serial, using a single CPU. Therefore, when analysing several time-frames and/or pressures, we recommend to use SLURM to run the analysis for multiple identical serial cases (each one representing a pressure) in parallel. The scripts to perform such analysis can be found in the folder *ANALYSIS\_MASTERFILES\_SLURM*

# Analysis

If you meet all the requirements (mentioned in the home address of this repository), then you simply have to execute the following command in a terminal **./bashMASTER.sh**. This will automatically execute each of the following five scripts, for which we provide below a brief description. A more detailed explanation can be found in the README files inside each of the three folder located here (*1\_network\_connectivity*, *2\_Loops*, *3\_Linking*). 

1. **bash.analysis1.networkconnectivity.sh**. This script copies the folder *1\_network\_connectivity* into each of the pressure folders and then it will execute the network connectivity analysis. It creates:
	* 1.1) **nc\_vs\_t.dat**.
	* 1.2) **traj.lammpstrj** 
	* 1.3) **network\_connectivity\_frameX.dat** (with X representing the corresponding frame).
	* 1.4) **frequency\_frameX.txt** (with X representing the corresponding frame).
	* 1.5) **Valence/frequency\_frameX.txt** (with X representing the corresponding frame).
	* 1.6) **Distributions/distprobAngleX** (with X representing the corresponding frame).
	* 1.7) **Distributions/distprobDistX**  (with X representing the corresponding frame).

2. **bash.analysis2.loops.mathematica.sh**. This script copies the folder *2\_Loops* into each of the pressure folders and then it executes the mathematica script *NminLoop.m* to find all the loops formed by water molecules. By default the analysis is performed only at the last frame (11) from simulations. The Mathematica script is executed by using the Mathematica kernel and not the usual graphical interface. Instructions on how to perform the analysis through the graphical interface are found below. When correctly executed, the mathematica script will create two files: 
	* 2.1) **Loops.txt**
	* 2.2) **attempts.txt**.

3. **bash.analysis3.loops.fractalDimension.sh**. This script moves the terminal into *2\_Loops* that can be found inside each of the pressure folders. Then it executes *bash\_NumberOfMinLoops.sh* to find all the loops. By default the analysis is performed only on the last frame (11). It creates:
	* 3.1) *1\_loopsFound* folder, here it stores **attempts.txt** and **NLoopsApprox2.txt** created in the following point.
	* 3.2) Runs *awk.remove\_repeated\_lopps.sh* and it creates **Loops\_cleaned.txt** with unrepeated loops.
	* 3.3) Splits original trajectory files into 11 different frames and store them in: *2\_splitTraj*
	* 3.4) Runs *fractalD\_ring\_v2.out* to create **fractalDimension2.dat**.
	* 3.5) computes histogram: **distprob**.
	* 3.6) computes **avel\_Rg.dat**.
	* 3.7) computes **NLoops.txt**, from which we compute **aveNLoops.dat**.
	* 3.8) Computes **average\_Rg\_atfixed\_l.dat**

4. **bash.analysis4.linking.IDsV2.sh**. This script copies the folder *3\_Linking* into each of the pressure folders and then it executes *bash\_DumpLoops.sh* that it extracts the particleID of all oxygens forming the loops. It creates:
	* 4.1) A file per time-frame called **LoopsParticleIDs\_tX** (with X the time-frame).

5. **bash.analysis5.linkingV2.sh**. This script moves the terminal into *3\_Linking* that can be found inside each of the pressure folders. Then it executes *bash\_computeLk.sh* to compute the linking number between all the pairs of loops previously found by our analysis. It creates:
	* 5.1) **Linking\_vs\_time.dat**
	* 5.2) **LinkingMatrix.dat**
	* 5.3) Two folders: *LinkedIDs* and *linkedRepeatedIDs*
	
6. **bash.analysis6.knotting.sh**. This script moves the terminal into *4\_Linking* that can be found inside each of the pressure folders. Then it executes the mathematica script *NminLoop\_v2.m* to find loops formed by exactly 25 water molecules (at the most 500 000 loops passing through each vertex). By default the analysis is performed only at the last frame (11) from simulations. The Mathematica script is executed by using the Mathematica kernel and not the usual graphical interface. Instructions on how to perform the analysis through the graphical interface are found below. When correctly executed, the mathematica script will create two files: 
	* 6.1) **Loops.txt**
	* 6.2) **attempts.txt**.

Then, the script bash.Extract3Dloops.sh is executed to extract the 3D-configurations of loops; followed by the *bash.kymoKnot.sh* script, which looks for knotted configurations of the loops. More information is found in the README file inside folder *4\_Linking*:
   
* 6.3) **centreline\_l25_tX.dat**
* 6.4) **BU\_\_centreline\_l25\_tX.dat**
* 6.5) **kknotAnalysis.txt**
* 6.6) **idFoundKnots.txt**
* 6.7) **ConfKnots.txt**

# Analysis when the Mathematica script is executed through the graphical interface

If the user prefers, it is also possible to execute the mathematica script manually, which will require the following modifications:

1. Open the file *bashMASTER.sh* (provided in the original repository) and comment lines 21 and 26: *#./bash.analysis1.networkconnectivity.sh ${p}* and *#./bash.analysis2.loops.mathematica.sh ${p}*.
2. Then copy manually folders *1\_network\_connectivity* and *2\_Loops* inside each of the pressure folders (at the same level where you find the file *fort.100*).
3. Go inside *1\_network\_connectivity* (for each of the pressures) and execute the C++ program by typing in a terminal: *./connectivity ../ ./ fort.100 2.2 30*
4. Go inside *2\_Loops* (for each of the pressures) and open the file *NminLoop\_v2.m* with Mathematica. Then, execute all the cells as usual, by pressing the keys *shift + enter*.
5. After executing the mathematica scripts, two files will be created **Loops.txt** and **attempts.txt** for each pressure. Finally, you can run the previously modified *bashMASTER.sh* file, to continue with the analysis.
