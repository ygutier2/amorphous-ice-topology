# Analysis Tools versions
Here you will find previous versions of the code and a brief description of the modifications made.

* **1. Version 2024-04-12**: This is the version used for the analysis shown with the initial submission of our manuscript. This is basically the same as the one included at the root of this repository, but when the code to compute the linking number between loops was less efficient. Since then, we modified the c++ script to compute the linking number between minimum loops **ComputeLinking_water.c++**.
> *LinkingMap* change from a 2D vector to an integer. This saves a lot of memory ram, particularly when dealing with several loops.
>
> The mn-elements of the linking matrix are now printed only with the linking between loops m and n is different from zero. This saves storage space.
>
> We don't do the loop to reconstruct the second loop starting from a different bead. Instead, we work-out a set of translations that allow to compute linking only once between every pair of loops. This saves computational time.
