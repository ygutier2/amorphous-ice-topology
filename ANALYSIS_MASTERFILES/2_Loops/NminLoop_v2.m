(* ::Package:: *)

(* ::Input::Initialization:: *)
(*Uncomment following line if executing through graphical interface*)
(*SetDirectory[NotebookDirectory[]];*)
(***********)
(*Variables*)
(***********)
(*The total number of particles in the simulation*)
Ntot=1536;
(*The number of frames to analyse. tf is fixed and ti in (1,11)*)
ti=11;
tf=11;
frames=(tf-ti)+1;
(*The maximum number of water molecules forming a loop*)
Nmax=13;
(*The total number of water molecules*)
M=Ntot/3;
(*The total number of data points*)
Ndata=M*frames;


(* ::Input::Initialization:: *)
(************************************************)
(*Analyse data from different snapshots in a loop*)
(************************************************)
For[t=0,t<frames,t++,
t1=ti+t;
data=Import["../1_network_connectivity/network_connectivity_frame"<>ToString[t1]<>".dat","Table"];

(*Molecules that are connected*)
datamol=Table[data[[i]][[1]]\[UndirectedEdge]data[[i]][[2]],{i,1,Length[data]}];
gmol=Graph[datamol,VertexLabels->None];

(*Create a list with all the vertices included in the graph gmol*)
(*This is done because, if a water molecule was not connected with any other one, then it was not included in the graph*)
vl=VertexList[gmol];

(********************************************************************)
(*Compute all the loops passing through each of the water-molecules.*)
(********************************************************************)
(*Track number of attempts, and Successfull and unsuccessful attempts to compute a loop*)
n=0;
nsuccess=0;
nfail=0;
Nloops=0;
While[n<M,
n++;
r1=n;
(*Check if r1 is a vertex in the graph*)
flag=0;
existr1=MemberQ[vl,r1];
If[existr1,flag++,nfail++;Continue[]];
(*If vertex exists*)
If[flag==1,
(*Create graph using only vertices larger or equal than r1*)
datamol1=Select[datamol,#[[1]]>=r1 && #[[2]]>=r1&];
gmol1=Graph[datamol1];
(*Find all cycles formed by at the most Nmax water-molecules and passes through vertex r1*)
fc=FindCycle[{gmol1,r1},Nmax,All];
(*Number of loops found*)
l=Length[fc];
If[l>0,
nsuccess++;
(*total number of loops*)
Nloops+=l;

m=0;
While[m<l,
m++;
(*The number of water molecules in the loop*)
lw=Length[fc[[m]]];
(*Remove the outter-most brackets from the mth-loop*)
flatf=Flatten[fc[[m]],1];
(*Obtain a list of the vertices that are part of the loops found*)
verlist=VertexList[flatf];
PutAppend[StringRiffle[{t1,lw,verlist}],"Loops.txt"];
]
,
(*If l is not larger than 0*)
nfail++;
Continue[]](*Close if statement on l*)
,
(*If r1 does not exist*)
Continue[];](*Close if statement on flag*)
](*Close while loop on attempts*)
PutAppend[StringRiffle[{t1,nsuccess,nfail,Nloops}],"attempts.txt"];
](*Close for loop on time*)
