#include<vector>     /*Library for using vector insted of arrays*/
#include<iostream>   /*Basic functions*/   
#include <cmath>     /*Define some fundamental mathematical operations*/
#include<stdlib.h>   /*Useful for random numbers*/
#include <sstream>
#include <fstream>   /*To read and write into files*/
#include <limits>
#include <stdio.h>   /*Basic functions in particular "sprintf"*/
#include <iomanip>      // std::setprecision
#include <tuple>     //Functions can return more than one value
#include <set>
#include <algorithm>



using namespace std;

#define pi 3.141592653589793238462643383279502884197169399375
#define SMALL 1e-20

int main(int argc, char *argv[])
{
    /**********************************************/
    /*Pass the following arguments to the programm*/
    /**********************************************/
    /*To get help screen of the program: ./trajectories help*/
    if(argc<4)
    {
        cout << "Four arguments are required by the programm:"              << endl;
        cout << "1.- Path to the input data"                                << endl;
        cout << "2.- Output path"                                           << endl;
        cout << "3.- Input file (without timestep)"                         << endl;
    }

if(argc==4)
{   
    cout << "ok" << endl;
    /*argv[1] Directory where the data is*/
    /*argv[2] Directory where the output will be stored*/
    /*argv[3] Input file*/

    /*timestep*/
    int timestep;
    
    //The total number of atoms (512 water molecules. 3 particles each)
    int Natoms=1536;
    
    //Each water molecule is made by two hydrogens and one oxigen.
    //So the number of particles per molecule is 3:
    int Nppmol=3;
    
    //The number of molecules: Natoms/Nppmol
    int M=Natoms/Nppmol;
    
    //Reads this information
    vector< vector< vector<float> > > position;
    position.resize(M, vector<vector<float>>(Nppmol, vector<float>(3)));
    
    vector< vector<int> > typevec;
    typevec.resize(M, vector<int>(Nppmol));
    
    vector< vector<int> > index;
    index.resize(M, vector<int>(Nppmol));
        
    //To cout the same number of decimals as in the files we are reading
    std::cout << std::fixed;
    std::cout << std::setprecision(7);

    //Useful variables to read the file
    float Lx,Ly,Lz;
    float x,y,z;
    char type;
    char type1 = 'O';
    char type2 = 'H';
    
    string line;
    
    ifstream inputFile;
    char readFile[400] = {'\0'};
    sprintf(readFile, "%s%s",argv[1],argv[3]);
    inputFile.open(readFile);
    
    //Error if file not found
    if (!inputFile) {
        std::cerr << "Error: Unable to open the file." << std::endl;
        return 1;
    }
    
 
    int frame=0;
    int ln=0;
    
    //Reads the file line by line
    while (std::getline(inputFile, line)) {
        std::istringstream ss(line);
        
        //Read First line
        if(ln==0){
            ss >> line;
        }
        
        //Second line contains the simulation box size in Angstroms
        if(ln==1){
            ss >> Lx >> Ly >> Lz;
            //cout << ln << " " << Lx << " " << Ly << " " << Lz << endl;
        }
        
        //The next Natoms lines contain the positions of particles
        if (ln>1){                
            //Try to extract type and position from each line
            if (ss >>  x >> y >> z) {                
                //Molecule id
                int m = floor((double)(ln-2)/(1.0*Nppmol));                                           
                position[m][(ln-2)%Nppmol][0] = x;
                position[m][(ln-2)%Nppmol][1] = y;
                position[m][(ln-2)%Nppmol][2] = z;
                
                //Particle index
                index[m][(ln-2)%Nppmol]=ln-1; 
                
                //It reads one Oxigen followed by two hydrogens
                //For oxigen particles
                if((ln-2)%3==0){
                    typevec[m][(ln-2)%Nppmol] = 0;
                }

                //For hydrogen particles
                if((ln-2)%3!=0){
                    typevec[m][(ln-2)%Nppmol] = 1;
                }            
            } 
        }
        
        //Line number increment
        ln++;
        
        
        //Check positions of molecules at a given frame:
        /*
        if(ln==Natoms+2){
            for(int m=0; m<M; m++){
                for(int n=0; n<Nppmol;n++){
              cout << index[m][n] << " " << m << " " << typevec[m][n] << " " << position[m][n][0] << " " << position[m][n][1] << " " << position[m][n][2] << endl;
                }
            }
        }
        */
        
        
        //After reading all the lines related to a frame:
        //1.- reset the line number.
        //2.- increase the frame count.
        //3.- Dump a file
        if(ln==Natoms+2){
            ln=0;
            frame++;
            //cout<< "Configuration for frame==" << frame << endl;
	    //

	    //In Fausto's Configurations: Lx,Ly,Lz in [o,L]
	    //Here we translate the coordinates so particles positions are in [-L/2.0,L/2.0]
	    float trxx=-Lx/2.0;
	    float tryy=-Ly/2.0;
	    float trzz=-Lz/2.0;
                                                    
            //////////////////////////////////////////////
            // PRINT a configuration file per time-frame
            //////////////////////////////////////////////
            char writeFile1[400] = {'\0'};
            sprintf(writeFile1, "%sConf%d",argv[2],frame);
            ofstream write1;
            write1.open(writeFile1); 
            

            write1 << "ITEM: TIMESTEP"            << endl;
            write1 << frame                       << endl;
            write1 << "ITEM: NUMBER OF ATOMS"     << endl;
            write1 << Natoms                      << endl;
            write1 << "ITEM: BOX BOUNDS pp pp pp" << endl;
            write1 << -Lx/2.0 << " " << Lx/2.0    << endl;
            write1 << -Ly/2.0 << " " << Ly/2.0    << endl;
            write1 << -Lz/2.0 << " " << Lz/2.0    << endl;
            write1 << "ITEM: ATOMS id mol type x y z" << endl;
            for(int m=0; m<M; m++){
              for(int n=0; n<Nppmol;n++){
                int id=index[m][n];
                int mol = floor((float)(id-1)/(1.0*Nppmol));
                char tipo;
                if(typevec[m][n]==0){tipo='O';}
                if(typevec[m][n]==1){tipo='H';}
            write1 << id << " " << mol+1 << " " << tipo << " " << position[m][n][0]+trxx << " " << position[m][n][1]+tryy << " " << position[m][n][2]+trzz << endl;
              }
            }
            write1.close();
            write1.clear();
            

        }//Close if statement after reading all the data from a single frame
    }//Close while loop after reading all frames
    
    //cout << "last line number: " << ln << endl;

    inputFile.close(); // Close the file after reading
             
}


return 0;
}
