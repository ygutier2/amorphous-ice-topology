# Loops analysis

## The mathematica notebook
The mathematica notebook **NminLoop\_v2.m** reads the data from the network connectivity files in folder **../1\_networkConnectivity/**. These files contain information about which water molecule (with  moleculeID $m\in[1,N]$) is connected with any other water molecule. The mathematica notebook contains comands to look for the loops passing throug the vertex/molecule $r_{1}$ and formed by $l \in [4,l_{m}]$ water molecules, with $l_{m}=13$ a cutoff size for the loops found by our analysis. To improve the efficiency of our analysis, after finding all the loops passing through $r_{1}$, we remove that vertex and continue our loop-search at vertex $r_{1}+1$. 

**IMPORTANT**: before running the mathematica notebook *NminLoop\_v2.m* you need to replace the value of the variables:

1. **Ntot**: the total number of particles in the simulation, for our case 512X3=1536.
3. **ti**: This is the initial frame to analyse. Since in our trajectory file we have 11 frames, then $t_{i} \in [1,11]$. 
2. **tf**: This is the final frame to analyse. The largest value it can take  should be equal to the number of files produced during the network connectivity analysis. In our example 11.
3. **Nmax**: Loops can be formed at the most by this number of water-molecules. In the paper *Nature Physics volume 18, pages 1248–1253 (2022)*, this is set to Nmax=13, and therefore is the value we use.

The analysis will create the file **Loops.txt** with columns:
> 
> 1.- frame
>
> 2.- The number of water molecules ($l$) connected and forming a loop 
>
> 3.- The next columns represent the molecule-id of all water molecules connected (in order) in the loop.

Also the file **attempts.txt** will be created, with columns:
>
> 1.- frame
>
> 2.- The number of successful attempts to find a loop.
>
> 3.- The number of unsuccessful attempts to find a loop
> 
> 4.- An approximate of the number of loops



## Fractal dimension
The bash script (**bash\_NumberOfMinLoops.sh**) contains the instructions to perform the following analysis:

> Runs the AWK script **awk.remove\_repeated\_lopps.sh** which creates the file *Loops\_cleaned.txt*. This is a copy of *Loops.txt* after removing repeated loops.
>
> Runs the C++ code **splitTraj.cpp** which splits the original trajectory file (*fort.100*) into 11 different configurations (ConfX, with X corresponding to the time-frame) and stores them in *2\_splitTraj*. **IMPORANT: note that during this step a translation was applied to all particles, so the centre of the box lies at the origin of coordinates.**
> 
> Runs the C++ code **fractalD\_ring\_v2.cpp** which reads the moleculeIDs from *Loops\_cleaned.txt* and identifies the 3D configuration of this path in our simulations (after reconstruction of the path using the minimum image criterion). It also computes the radius of gyration ($R_{g}$) of the loops. To avoid considering linear paths closed through the periodic boundary conditions as a loop, we imposed the condition $R_{g}$<= L/3.0 Angstroms. This analysis creates the file *fractalDimension2.dat*, with $3+l$ columns:
>
>> 1.- Timestep.
>>
>> 2.- The number of water molecules ($l$) in the loop.
>>
>> 3.- The radius of gyration of the loop.
>>
>> 4.- From column 4 onwards: the molecule-id of the water molecules forming the loop.
>
> Runs the AWK script (**awk.histbin**), which computes the probability distribution $P(l)$ of observing in the simulation loops made by $l$ water molecules. This informations is dumped to the file *distprob*.
>
> Using AWK to analyse the data in *fractalDimension2.dat*, it creates the file *avel\_Rg.dat* with four columns:
>
>> 1.- <$l$>.
>>
>> 2.- Standard deviation of $l$.
>>
>> 3.- <$R_{g}$>
>>
>> 4.- Standard deviation of $R_{g}$.
>
>Runs the C++ code **loops\_distribution.cpp** to compute the total number of unrepeted loops as function of time (*NLoops.txt*) and also the average over time and standard deviation of the number of loops (*aveNLoops.dat*).
>
> Runs the script **awk.ave\_at\_same\_l.sh** which creates the file *average\_Rg\_atfixed\_l.dat* with the following columns:.
>> 1.- $l$
>>
>> 2.- <$R_{g}$>, where the average is taken over all loops with the same $l$
>>
>> 3.- Standard deviation of the radius of gyration.
