#!/bin/bash
  #What is the pressure you want to analyse:
  p="$1"
  
  #Current working directory
  cwd=$(pwd)
  
  
  ##################################################
  ## ANALYSIS: Fractal dimension of minimum loops ##
  ##################################################
  echo "Running Fractal Dimension at P=${p}"
      
  #move terminal to the analysis folder
  cd ../Pressure_${p}/2_Loops/

  #Run bash with further instructions in the terminal
  ./bash_NumberOfMinLoops.sh
      
  #Move back to MASTER folder
  cd "${cwd}"
