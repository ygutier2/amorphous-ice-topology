# Linking number analysis
**Important:** The file *fractalDimension2.dat* (produced by the analysis in the folder *2\_Loops*) is needed for the computation of the linkng number.

The bash script (**bash\_DumpMinLoops.sh**) contains the instructions to read the moleculeID of all particles in the loops (from *fractalDimension2.dat*) and to find the particleID of all oxygens in the loop. After running the script, this information is stored inside the folder *1\_dataLoop*. There, you will find a file per frame (called *LoopsParticleIDs\_tX.dat*, with X corresponding to the time-frame) containing in the first column the number of oxygens in the loop, and in the following columns the particleIDs of all oxygens in that loop.

A second bash script (**bash\_computeLk.sh**) contains the instructions to run the the C++ code **ComputeLinking\_water.c++**, which reads the particleIDs of oxygens in the loops obtained previously, and also the snapshots from simulations in the folder *../2\_Loops/2\_splitTraj/*. **IMPORTANT:** the variables $t_{i}$, $t_{f}$ and $d_{freq}$ of this section (lines 3-5 in the bash script) should be the same as the ones used in the mathematica notebook. This analysis will create two files:

> *Linking\_vs\_time.dat*, which contains five columns:
>
>> 1.- The frame of the calculation.
>>
>> 2.- The total number of loops.
>>
>> 3.- The sum of the absolute value of the linking number between all pairs of loops.
>>
>> 4.- The nature of our system makes possible that two minimum loops could share a segment of oxygens. We decided to count the number of loops that share six or more oxygens. The result of this count is stored in this column.
>>
>> 5.- The contribution to the total linking number by these rings that share oxygens is stored in column five.
>
> *LinkingMatrix.dat* with four columns representing:
>> 1.- The time frame.
>>
>> 2-3.- The ringIDS of the two rings used for the calculation of the linking number.
>>
>> 4.- The value of the linking number found between the two previous rings.

We note that in the bash script **bash\_computeLk.sh** and additional variable $t_{d}$ appears in line 9. The user needs to set this variable with the time-frame at which additional information is printed, then two folder will be created:

> 1.- *linkedIDS*: At the time-frame $t_{d}$ a unique file is created, for instance "LinkedIDs\_11.dat". This file contains which rings are linked, and the particle IDs of the oxygens of the two linked rings. With this information it is possible to visualize the linked rings in the original simulation with VMD, just follow these steps in a terminal:
>
>>1.1) Use vmd to open the trajectory from simulation. Assuming that the terminal path is located in this folder you have to type: *vmd ../1\_network\_connectivity/traj.lammpstrj*
>>
>>1.2) To visualized unwrapped coordinates: in the VMD menu select: *Extensions/TK Console* and type in the console *pbc wrap -all*
>>
>>1.3) In the VMD screen move to frame 11.
>>
>>1.4) In the VMD menu select: *Graphics/Representations/DrawingMethod/VDW*. Then decrease sphere scale to 0.8.
>>
>>1.5) *CreateRep*: serial XX XX XX .These are the particleIDS of the first minimum loop. Linked to this, there is a second minimum loop. To visualize it we have to *CreateRep*: serial YY YY YY YY. The particleIDs of all pairs of loops that are linked can be found in the file *LinkedIDs\_11.dat*.
>
>2.- *linkedRepeatedIDS*: At the time-frame $t_{d}$ a unique file is created, for instance "LinkedRepeatedIDs\_11.dat". This file contains the IDs of rings that have more than 6 oxygens shared with any other ring. If these ring are linked with any other, we also print the particle IDs of the oxygens of the two linked rings, in a similar way than the previous point.