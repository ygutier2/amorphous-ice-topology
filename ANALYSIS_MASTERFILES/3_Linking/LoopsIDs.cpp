#include<vector>     /*Library for using vector insted of arrays*/
#include<iostream>   /*Basic functions*/   
#include <cmath>     /*Define some fundamental mathematical operations*/
#include<stdlib.h>   /*Useful for random numbers*/
#include <sstream>
#include <fstream>   /*To read and write into files*/
#include <limits>
#include <stdio.h>   /*Basic functions in particular "sprintf"*/
#include <iomanip>      // std::setprecision
#include <tuple>     //Functions can return more than one value
#include <set>
#include <algorithm>



using namespace std;

#define pi 3.141592653589793238462643383279502884197169399375
#define SMALL 1e-20

int main(int argc, char *argv[])
{
    //cout << "ok" << endl;
    /*argv[1] Directory where the data is*/
    /*argv[2] Directory where the output will be stored*/
    /*argv[3] Input file*/
    
    /*time-frame*/
    int frame;
    frame = atoi(argv[4]);
    
    /*Cutoff distance between H(A)--O(B), in Angstroms*/
    float rc;
    rc = atof(argv[5]);
    
    /*Cutoff angle between O(B)O(A)-axis and H(A)O(B)-bond, in degrees*/
    float ac;
    ac = atof(argv[6]);
    
    /*Number of molecules forming the loop*/
    int l;
    l = atoi(argv[7]);


    /*Molecule-id of the water-molecules connected, in total there are l*/
    vector <int> Molid(l);

    //Read the molecule ids
    for(int i=0; i<l; i++){
        Molid[i] = atoi(argv[8+i]);        
    }

    /*Particle-id of the oxygens in the loop*/
    vector <int> Particleid(l);
    for(int i=0; i<l; i++){
        Particleid[i]=(3*Molid[i])-2;
    }

    
    //The total number of atoms
    int Natoms;
    
    //Each water molecule is made by two hydrogens and one oxigen.
    //So the number of particles per molecule is 3:
    int Nppmol=3;
    
    //The number of molecules: Natoms/Nppmol
    int M;
            
        
    //Declare vectors to store the information of all the particles In the system.
    //These vectors will be re-sized and initialized below
    vector< vector< vector<float> > > position;
    vector< vector<int> > typevec;
    vector< vector<int> > indice;
        
    //Stores the oxigen and two hydrogens in each of the water molecules forming the loop
    vector< vector< vector<double> > > posLoop(l, vector<vector<double> >(Nppmol, vector<double>(3)));
    vector< vector<int> > typeLoop(l, vector<int>(Nppmol));
    vector< vector<int> > indiceLoop(l, vector<int>(Nppmol));
    
    //Only position and atomID of patches of the molecules forming the ring
    int Nh=2;
    vector< vector< vector<double> > > posH(l, vector<vector<double> >(Nh, vector<double>(3)));
    vector< vector<int> > index(l, vector<int>(Nh));

        
    /*the following variables will be useful to read the data file*/
    int time;
    int id,mol;
    char type;
    char type1 = 'O';
    char type2 = 'H';
    double x,y,z;
    double lmin,lmax,Lx,Ly,Lz;
    
    /*Read the file from simulations*/
    ifstream indata1;
    char readFile1[400] = {'\0'};
    string dummy;
    string line;
    sprintf(readFile1, "%s%s%d",argv[1],argv[3],frame);
    indata1.open(readFile1);
    
    
    if (indata1.is_open())
    {       
        //Read headers
        for(int i=0;i<10;i++){
            if(i==1) {
                indata1 >> time;
                //cout << "time " << time <<endl;
            }

            if(i==3) {
                indata1 >> Natoms;
                
                //The total number of molecules:
                M=Natoms/Nppmol;
                //cout << i << " " << Natoms<< endl;
            
                //Resize the vectors to store positions and type.
                position.resize(M, vector<vector<float>>(Nppmol, vector<float>(3)));
                typevec.resize(M, vector<int>(Nppmol));
                indice.resize(M, vector<int>(Nppmol));
            }

            if(i==5) {
                indata1 >> lmin >> lmax;
                Lx = lmax-lmin;
                //cout << "Lx " << Lx <<endl;
            }

            if(i==6) {
                indata1 >> lmin >> lmax;
                Ly = lmax-lmin;
                //cout << "Ly " << Ly <<endl;
            }

                if(i==7) {
                indata1 >> lmin >> lmax;
                Lz = lmax-lmin;
                //cout << "Lz " << Lz<<endl;
            }

                else getline(indata1,dummy);
        }
        
        //READ all ATOMS
        for(int n=0; n<Natoms; n++){
            indata1 >> id >> mol >> type >> x >> y >> z;
            
            //cout << id << " " << type << " " << x << " " << y << " " << z << endl;
                        
            int m = floor((double)(id-1)/(1.0*Nppmol));
            
            position[m][(id-1)%Nppmol][0] = x;
            position[m][(id-1)%Nppmol][1] = y;
            position[m][(id-1)%Nppmol][2] = z;
            
            indice[m][(id-1)%Nppmol] = id;
            
            //For oxigen particles
            if(type==type1){                
                typevec[m][(id-1)%Nppmol] = 0;
            }

            //For hydrogen particles
            if(type==type2){
                typevec[m][(id-1)%Nppmol] = 1;
            }
        }
        
        indata1.close();        
        indata1.clear();
        
    }
   
    else {cout << "Error opening file" << endl;}
    
/*    
    for(int m=0; m<M; m++){
        for(int n=0; n<Nppmol;n++){
           cout << indice[m][n] << " " << typevec[m][n] << " " << position[m][n][0] << " " << position[m][n][1] << " " << position[m][n][2] << endl;
        }
    }
*/   
   
    //////////////////////////////////////////////////////////////////////////////
    //Extract information of loops both, oxygens and hydrogens that are part of a loop//
    //////////////////////////////////////////////////////////////////////////////
    for(int i=0; i<l; i++){
       int m=Molid[i];
       for(int n=0; n<Nppmol; n++){
           //Contains both oxigen and hydrogens
           posLoop[i][n][0]=position[m-1][n][0];
           posLoop[i][n][1]=position[m-1][n][1];
           posLoop[i][n][2]=position[m-1][n][2];
           typeLoop[i][n]  =typevec[m-1][n];
           indiceLoop[i][n]=indice[m-1][n];           
       }
    }      

/*
    for(int m=0; m<l; m++){
        for(int n=0; n<Nppmol;n++){
           cout << indiceLoop[m][n] << " " << posLoop[m][n][0] << " " << posLoop[m][n][1] << " " << posLoop[m][n][2] << endl;
        }
    }
*/    
    
    
    /////////////////////////////////////////////////
    //Create the path with the previous information//
    /////////////////////////////////////////////////
    vector<int> pathLoop;
    for(int m=0; m<l; m++){
        //oxygen index
        int idox=indiceLoop[m][0];
                              
        //Add to path
        pathLoop.push_back(idox);
    }
    
       
    /////////////////////////////////////////////////////////
    // PRINT The id of all the particles connected to a loop
    /////////////////////////////////////////////////////////
    char writeFile2[400] = {'\0'};
    sprintf(writeFile2, "%sLoopsParticleIDs_t%d.dat",argv[2],frame);
    
    ofstream write2;
    write2.open(writeFile2, std::ios_base::app);
    
    for(int m=0; m<l; m++){
        //The total number of particles forming the loop
        int Nt=l;
        
        if(m==0){write2 << Nt << " ";}       
        
        if(m<l-1){write2  << pathLoop[m] << " ";}        
        if(m==l-1){write2 << pathLoop[m];}
    }
    
    write2 << endl;
    write2.close();
    write2.clear();
 

return 0;
}
