#!/bin/bash  
  #IMPORTANT: the parameters here should be the same as the ones inthe mathematica script:
  ti=11
  tf=11
  dfreq=1
  
  #The frame at which you want to dump the linked configurations found by this analysis.
  #Set to -1 if you don't want to dump the configurations 
  td=11

  #Compile the programm:
  #c++ -std=c++11 ComputeLinking_water.c++ -o ComputeLinking_water

  #Create a folder in which to store the pair of rings that are linked
  if [ -f "LinkingMatrix.dat" ] ; then
    rm "LinkingMatrix.dat"
    rm "Linking_vs_time.dat"
    rm -r linkedConfigurations
    rm -r linkedIDS
    rm -r linkedRepeatedIDS
  fi
  
  if [ ${td} != -1 ] ; then
    mkdir -p linkedConfigurations
    mkdir -p linkedIDS
    mkdir -p linkedRepeatedIDS
  fi
  
  
  for t in $( seq ${ti} ${dfreq} ${tf})
  do
     #1.- Path to the input data
     datapath="../2_Loops/2_splitTraj/"
    
     #2.- Output path
     outputpath="./"
        
     #3.- Input file with configuration information (without timestep)
     rfile="Conf"
          
    ./ComputeLinking_water ${datapath} ${outputpath} ${rfile} ${t} ${td}
       
  done

  
  rm ComputeLinking_water
