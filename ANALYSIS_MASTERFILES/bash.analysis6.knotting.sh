#!/bin/bash
  #What is the pressure you want to analyse:
  p="$1"
  
  #Current working directory
  cwd=$(pwd)
  
  
  #################################
  ## ANALYSIS DURING COMPRESSION ##
  #################################
  echo "Knotting during compression at P=${p}"
      
  #move terminal to the analysis folder
  cd ../Pressure_${p}/
  if [ -e "4_Knotting" ]; then
     rm -r 4_Knotting
  fi
  
  #Copy folder to the corresponding directory
  cp -r "${cwd}/4_Knotting" .
  cd 4_Knotting
  
  #Run mathematica script in the terminal
  math -script NminLoop_v2.m
                                    
  #Run bash to extract the position of particles in the loops
  ./bash.Extract3Dloops.sh
  
  #Run bash to use kymoknot to analyse the 3Dpaths of loops and look for knots.
  ./bash.kymoKnot.sh
      
  #Move back to MASTER folder
  cd "${cwd}"
