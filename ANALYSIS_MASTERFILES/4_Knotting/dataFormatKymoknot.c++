#include<vector>     /*Library for using vector insted of arrays*/
#include<iostream>   /*Basic functions*/   
#include <cmath>     /*Define some fundamental mathematical operations*/
#include<stdlib.h>   /*Useful for random numbers*/
#include <sstream>
#include <fstream>   /*To read and write into files*/
#include <limits>
#include <stdio.h>   /*Basic functions in particular "sprintf"*/
#include <iomanip>      // std::setprecision
#include <tuple>     //Functions can return more than one value
#include <set>
#include <algorithm>



using namespace std;

#define pi 3.141592653589793238462643383279502884197169399375
#define SMALL 1e-20

int main(int argc, char *argv[])
{
/*argv[1] Directory where the data is*/
/*argv[2] Directory where the output will be stored*/
/*argv[3] Input file*/

//In a hydrogen-bond, the inter-oxygen distance is smaller than 3.5 Angstroms. Therefore
double rc=3.5;

//We found loops made by 3-13 water molecules. Therefore, we expect that
//the Rg of these loops is always smaller than Rgmax=L/3. If larger, it represents
//a sequence of water-molecules linked through the Periodic Boundary Conditions and not a loop
double Rgmax;

//Loops formed by less than 4 water-molecules are not loops
int Nlmin=4;

/*time-frame*/
int frame;
frame = atoi(argv[4]);
    
/*Number of molecules forming the loop*/
int l;
l = atoi(argv[5]);

if(l>=Nlmin){
  
/*Molecule-id of the water-molecules connected, in total there are l*/
vector <int> Molid(l);
    //Read the molecule ids
    for(int i=0; i<l; i++){
        Molid[i] = atoi(argv[6+i]);
    }

/*Particle-id of the oxygens in the loop*/
vector <int> Particleid(l);
    for(int i=0; i<l; i++){
        Particleid[i]=(3*Molid[i])-2;
    }
    
//Here we will store the position of the consecutive connected Oxygens
vector< vector<double> > position(3, vector<double>(l));
vector< vector<double> > originalposition(3, vector<double>(l));


/*the following variables will be useful to read the data file*/
long long int time;
long int Ntot;   
long int id,mol;
char type;
double x,y,z;
double lmin,lmax,Lx,Ly,Lz;    
    
/*Read the file from simulations*/
ifstream indata1;
char readFile1[400] = {'\0'};
string dummy;
string line;
sprintf(readFile1, "%s%s%d",argv[1],argv[3],frame);
indata1.open(readFile1);

if (indata1.is_open())
{  
    //READ INFO
    int i=0;
    int fflag=0;
    while (!indata1.eof()){
        //Read headers
        if(i==1) {
            indata1 >> time;
            //cout << "time " << time <<endl;                    
        }
            
        if(i==3) {
            indata1 >> Ntot;
            //cout << "Ntot " << Ntot << endl;
        }
            
        if(i==5) {
            indata1 >> lmin >> lmax;
            Lx = lmax-lmin;
            //cout << "Lx " << Lx <<endl;
        }

        if(i==6) {
            indata1 >> lmin >> lmax;
            Ly = lmax-lmin;
            //cout << "Ly " << Ly <<endl;
        }

        if(i==7) {
            indata1 >> lmin >> lmax;
            Lz = lmax-lmin;
            //cout << "Lz " << Lz<<endl;
        }
            
        //READ ATOMS
        if(i>=9) {
            indata1 >> id >> mol >> type >> x >> y >> z;
            
            //cout << id << " " << mol << " " << type << " " << x << " " << y << " " << z << endl;
            
            //Check if the id corresponds to the oxygens that are connected into a loop
            for(int j=0; j<l;j++){
                 if(Particleid[j]==id){
                     position[0][j] = x;
                     position[1][j] = y;
                     position[2][j] = z;
                     fflag+=1;
                     originalposition[0][j]=x;
                     originalposition[1][j]=y;
                     originalposition[2][j]=z;
                 }
            }
            
            //Exit the while loop if you found all the loop information
            if(fflag==l){break;}
        }

        else getline(indata1,dummy);
            
        i+=1;
    }
    indata1.close();        
    indata1.clear();  
}
   
else{cout<<"error reading file: " << readFile1 << endl;}


//SET THE VALUE OF THE MAXIMUM RADIUS OF GYRATION THAT A LOOP COULD HAVE
Rgmax=Lx/3.0;

/*
  //Check
  for(int i=0; i<l; i++){
        cout << Molid[i] << " ";
  }
  cout << endl;
  for(int i=0; i<l; i++){
        cout << Particleid[i] << " ";
  }
  cout << endl;
*/

//////////////////////////////////////////////
//Set correct coordinates of Oxigen in the path
//////////////////////////////////////////////
for(int i=0; i<l-1; i++){
    double dx = position[0][i+1] - position[0][i];
    double dy = position[1][i+1] - position[1][i];
    double dz = position[2][i+1] - position[2][i];
    
    double dist=sqrt(dx*dx + dy*dy + dz*dz);

    if(dist>rc){
    	/*Minimum Image criterion*/
    	if (dx > 0.5*Lx){ dx -= Lx;}
        if (dy > 0.5*Ly){ dy -= Ly;}
   	    if (dz > 0.5*Lz){ dz -= Lz;}

   	    if (dx <= -0.5*Lx){ dx += Lx;}
   	    if (dy <= -0.5*Ly){ dy += Ly;}
   	    if (dz <= -0.5*Lz){ dz += Lz;}

   	    //change the position of the i+1
   	    position[0][i+1] = position[0][i]+dx;
   	    position[1][i+1] = position[1][i]+dy;
 	    position[2][i+1] = position[2][i]+dz;
    }
}

     
////////////////////////////////////////////////
//POSITIONS OF RECONSTRUCTED LOOPS THROUGH PBC//
////////////////////////////////////////////////
char writeFile1[400] = {'\0'};
sprintf(writeFile1, "%scentreline_l%d_t%d.dat",argv[2],l,frame);
ofstream write1;
write1.open(writeFile1, std::ios_base::app);

//Number of particles in the loop
write1 << l+1 << endl;
        
//Positions
for(int m=0; m<l;m++){
    write1 << position[0][m] << " " << position[1][m] << " " << position[2][m] << endl;
}
        
//Last and first need to be the same
write1 << position[0][0] << " " << position[1][0] << " " << position[2][0] << endl;    
write1.close();
write1.clear();




}//Close if statement to ensure that loops are made by Nlmin=4 water-molecules or more



return 0;
}
