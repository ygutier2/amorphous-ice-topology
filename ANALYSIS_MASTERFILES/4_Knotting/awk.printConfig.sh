#!/bin/bash

#Recieve the starting and ending lines to copy
l1="$1"
l1=$((l1+1))
l2="$2"

#System details
lmax=25

#File to store 3D configuration of the knots found
Confknots="ConfKnots.txt"

sed -n "${l1},${l2}p" centreline_l${lmax}_t11.dat >> ${Confknots}
echo "" >> ${Confknots}
echo "" >> ${Confknots}

