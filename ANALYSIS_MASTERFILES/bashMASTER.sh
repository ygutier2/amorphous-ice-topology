#!/bin/bash

#Array with pressures to analyse during compression.
#To convert to GPa just have to divide by 10000
declare -a pvec=("100" "4000" "8000" "12000" "16000" "20000")

#Print date and time when the analysis started
pdt1=$(date '+%d/%m/%Y %H:%M:%S');
echo "Starting time: $pdt1"

## now loop through the above array
for p in "${pvec[@]}"
do
   echo "Pressure: $p"

   #1.- Create network connectivity
   ./bash.analysis1.networkconnectivity.sh ${p}
   echo ""
   
   #2.- Mathematica notebook to find loops made of up-to 13 water molecules
   echo "Mathematica notebook"
   ./bash.analysis2.loops.mathematica.sh ${p}
   echo "Finish Mathematica notebook"
   echo ""

   #3.- Compute <l>, <Rg> and fractal dimension of the network
   echo "FractalD"
   ./bash.analysis3.loops.fractalDimension.sh ${p}
   echo "finish FractalD"
   echo ""
   
   #4.- 
   echo "Extracting IDs of Oxigens in loops"
   ./bash.analysis4.linking.IDsV2.sh ${p}
   echo "Finish Extracting IDs of oxygens in loops"
   echo ""
   
   #5.-
   echo "Computing Lk"
   ./bash.analysis5.linkingV2.sh ${p}
   echo "Finish Computing Lk"
   echo ""
   
   #6.-
#   echo "Computing Knotting"
#   ./bash.analysis6.knotting.sh ${p}
#   echo "Finish Computing Knotting"
#   echo ""
   
   echo ""
   echo ""
done

#Print date and time when the analysis finished
pdt2=$(date '+%d/%m/%Y %H:%M:%S');
echo "Ending time: $pdt2"
